import { Component, OnInit } from '@angular/core';
import { Monster, MonsterService, Skill, SkillService } from '../service/index';
import { Leader, LeaderService, Arousal, ArousalService } from '../service/index';

import { fadeInAnimation } from '../lib/animation/fade-in.animation';

@Component ({
    selector: 'pazz-dashboard',
    templateUrl: `./dashboard.component.html`,
    animations: [fadeInAnimation]
})

export class DashboardComponent implements OnInit {
    monsters: Monster[] = [];
    skills: Skill[] = [];
    leaders: Leader[] = [];
    arousals: Arousal[] = [];
    counts: number[] = new Array(0);

    start = 0;
    len = 10;

    constructor(
        private monsterService: MonsterService,
        private skillService: SkillService,
        private leaderService: LeaderService,
        private arousalService: ArousalService
    ) {

    }


    ngOnInit(): void {
        this.getMonster();
        this.getSkill();
        this.getLeader();
        this.getArousal();
    }
    getMonster(): void {
        this.monsterService.getMonsters()
        .then((monsters: Monster[]) => this.monsters = monsters)
        .then((monsters: Monster[]) => this.counts = new Array(Math.ceil(monsters.length / 10)) );
    }
    getSkill(): void {
        this.skillService.getSkills()
        .then((skills: Skill[]) => this.skills = skills);
    }
    getLeader(): void {
        this.leaderService.getLeaders()
        .then((leaders: Leader[]) => this.leaders = leaders);
    }
    getArousal(): void {
        this.arousalService.getArousals()
        .then((arousals: Arousal[]) => this.arousals = arousals);
    }

}
