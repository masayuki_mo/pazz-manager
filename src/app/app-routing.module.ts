import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { MonsterComponent, MonsterAddComponent, MonsterDetailComponent} from './monster/index';
import { LeaderComponent, LeaderAddComponent, LeaderDetailComponent } from './leader/index';
import { SkillComponent, SkillAddComponent, SkillDetailComponent } from './skill/index';
import { ArousalComponent, ArousalAddComponent, ArousalDetailComponent } from './arousal/index';
import { ManagerComponent, UserManagerComponent, GroupManagerComponent } from './_manage/index';
import { DashboardComponent } from './dashboard/index';

import { LoginComponent } from './_gard/login.component';
import { AuthGuard } from './_gard/auth.gard';
// import { AdminGuard } from './_gard/admin.gard';

const routes: Routes = [
    {
        path: '',
        component:  DashboardComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'login',
        component: LoginComponent
    },
    /*{
        path:   '',
        redirectTo: 'dashboard',
        pathMatch:  'full'
    },*/
    {
        path:   'dashboard',
        component:  DashboardComponent,
        canActivate: [AuthGuard]
    },
    {   path:   'monster',
        component: MonsterComponent,
        children: [
            { path: 'add', component: MonsterAddComponent },
            { path: 'detail/:id', component: MonsterDetailComponent }
        ],
        canActivate: [AuthGuard]
    },
    {   path:   'skill',
        component: SkillComponent,
        children: [
            { path: 'add', component: SkillAddComponent},
            { path: 'detail/:id', component: SkillDetailComponent }
        ],
        canActivate: [AuthGuard]
    },
    {   path:   'leader',
        component: LeaderComponent,
        children: [
            { path: 'add', component: LeaderAddComponent },
            { path: 'detail/:id', component: LeaderDetailComponent }
        ],
        canActivate: [AuthGuard]
    },
    {   path:   'arousal',
        component: ArousalComponent,
        children: [
            { path: 'add', component: ArousalAddComponent },
            { path: 'detail/:id', component: ArousalDetailComponent }
        ],
        canActivate: [AuthGuard]
    },
    {   path:   'admin',
    component: ManagerComponent,
    children: [
        { path: 'user', component: UserManagerComponent },
        { path: 'group', component: GroupManagerComponent }
    ],
    canActivate: [AuthGuard]
},
    {
        path: '**',
        redirectTo: 'dashboard'
    }
];
@NgModule({
    imports:    [
        RouterModule.forRoot (routes)
    ],
    exports: [ RouterModule ]
})
export class AppRoutingModule { }
