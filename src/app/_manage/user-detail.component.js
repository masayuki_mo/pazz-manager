"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var common_1 = require("@angular/common");
require("rxjs/add/operator/switchMap");
// Import Service
var index_1 = require("../service/index");
// Import Local Share Service
var subjects_service_1 = require("../service/subjects.service");
// Import Animation Component
var slide_in_out_animation_1 = require("../lib/animation/slide-in-out.animation");
var UserDetailComponent = (function () {
    function UserDetailComponent(userService, route, router, location, subjectsService) {
        this.userService = userService;
        this.route = route;
        this.router = router;
        this.location = location;
        this.subjectsService = subjectsService;
        this.user = new index_1.User;
        this.onCtrl = false;
    }
    UserDetailComponent.prototype.ngOnInit = function () {
        this.getChMsg();
    };
    UserDetailComponent.prototype.getChMsg = function () {
        this.user = this.userService.getDetail();
    };
    UserDetailComponent.prototype.save = function () {
        var _this = this;
        if (!this.user.Password && this.user.Name) {
            return;
        }
        this.userService.create(this.user)
            .then(function () {
            _this.subjectsService.publish('user-updated');
            _this.goBack();
        });
    };
    UserDetailComponent.prototype.goBack = function () {
        this.location.back();
    };
    UserDetailComponent.prototype.onKeydownHandler = function (event) {
        console.log(event);
        var x = event.keyCode;
        if (x === 116 || this.onCtrl && x === 82) {
            return this.refreshNavigation();
        }
        if (x === 224 || x === 17) {
            this.onCtrl = true;
        }
    };
    UserDetailComponent.prototype.refreshNavigation = function () {
        console.log('KeyDown!');
        this.router.navigate(['/arousal']);
        return false;
    };
    UserDetailComponent.prototype.onKeyupHandler = function (event) {
        var x = event.keyCode;
        this.onCtrl = (x === 224 || x === 17) ? false : false;
    };
    return UserDetailComponent;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", index_1.User)
], UserDetailComponent.prototype, "userProperty", void 0);
__decorate([
    core_1.HostListener('document:keydown', ['$event']),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [KeyboardEvent]),
    __metadata("design:returntype", Boolean)
], UserDetailComponent.prototype, "onKeydownHandler", null);
__decorate([
    core_1.HostListener('document:keyup', ['$event']),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [KeyboardEvent]),
    __metadata("design:returntype", void 0)
], UserDetailComponent.prototype, "onKeyupHandler", null);
UserDetailComponent = __decorate([
    core_1.Component({
        selector: 'user-detail',
        templateUrl: './user-detail.component.html',
        animations: [slide_in_out_animation_1.slideInOutAnimation],
        host: { '[@slideInOutAnimation]': '' }
    }),
    __metadata("design:paramtypes", [index_1.UserService,
        router_1.ActivatedRoute,
        router_1.Router,
        common_1.Location,
        subjects_service_1.SubjectsService])
], UserDetailComponent);
exports.UserDetailComponent = UserDetailComponent;
//# sourceMappingURL=user-detail.component.js.map