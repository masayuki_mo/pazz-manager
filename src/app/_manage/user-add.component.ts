import 'rxjs/add/operator/switchMap';
import { Component, HostListener } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
// Import Service
import { User, UserService } from '../service/index';
// Import Local Share Service
import { LocalDataService } from '../service/local-data.service';
import { SubjectsService } from '../service/subjects.service';
// Import Animation Component
import { slideInOutAnimation } from '../lib/animation/slide-in-out.animation';

@Component({
  selector: 'user-add',
  templateUrl: './user-add.component.html',
  animations: [slideInOutAnimation],
  host: { '[@slideInOutAnimation]': '' }
})

export class UserAddComponent {

    user: User = new User;
    onCtrl: boolean = false;

    constructor(
        private userService: UserService,
        private route: ActivatedRoute,
        private router: Router,
        private location: Location,
        private localDataService: LocalDataService,
        private subjectsService: SubjectsService
    ) {}

    add(): void {
        if (!this.user.Password && this.user.Name) { return; }
        this.userService.create(this.user)
            .then(() => {
                this.subjectsService.publish('user-updated');
                this.goBack();
            });
    }
    goBack(): void {
        this.location.back();
    }
    @HostListener('document:keydown', ['$event'])
    onKeydownHandler(event: KeyboardEvent): boolean {
        console.log(event);
        let x = event.keyCode;
        if (x === 116 || this.onCtrl && x === 82) {
            return this.refreshNavigation();
        }
        if (x === 224 || x === 17) {
            this.onCtrl = true;
        }

    }

    refreshNavigation(): boolean {
        console.log('KeyDown!');
        this.router.navigate(['/arousal']);
        return false;
    }

    @HostListener('document:keyup', ['$event'])
    onKeyupHandler(event: KeyboardEvent): void {
        const x = event.keyCode;
        this.onCtrl = (x === 224 || x === 17) ? false : false;
    }
}
