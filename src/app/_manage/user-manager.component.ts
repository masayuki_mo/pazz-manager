import { Component, OnInit, HostListener } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import 'rxjs/add/operator/switchMap';
// Import Component
import { UserAddComponent, UserDetailComponent } from './index';
// Import Service
import { User, UserService} from '../service/index';
// Import Local Share Service
import { LocalDataService } from '../service/local-data.service';
import { SubjectsService } from '../service/subjects.service';
// Import Animation Component
import { slideInOutAnimation } from '../lib/animation/slide-in-out.animation';
import { slideContentAnimation } from '../lib/animation/slide-content.animation';

@Component({
  selector: 'user-manager',
  templateUrl: './user-manager.component.html',
  providers: [ LocalDataService ],
  animations: [slideInOutAnimation, slideContentAnimation],
  host: { '[@slideInOutAnimation]': '' }
})

export class UserManagerComponent implements OnInit {

    users: User[] = [];
    onCtrl: boolean = false;

    searchOn: boolean = true;
    searchType: string = 'none';

    subWindow: string = 'void';
    subFlag: boolean = true;
    subTop: string   = '0px';
    subLeft: string  = '0px';

    subComponent: any;
    subComponents: any[] = [
        UserDetailComponent,
        UserAddComponent
    ];

    constructor(
        private router: Router,
        private location: Location,
        private localDataService: LocalDataService,
        private subjectsService: SubjectsService,
        private userService: UserService
    ) {}
    ngOnInit(): void {
        this.getUser();
    }
    getChMsg(): any {
        this.localDataService.toParentData$.subscribe(
            msg => {
                if (this.searchType === 'user') {
                    console.log(msg.id);
                    this.users[msg.id] = msg;
                }
                // this.closeSearch();
            }
        );
    }
    closeSubConponent(): void {
        this.subWindow = 'hide';
        this.subFlag = true;
        this.subTop = '';
        this.subLeft = '';
    }
    getUser(): void {
        this.userService.getAll()
            .then((users: User[]) => this.users = users);
    }

    showDetail(id: number): void {
        if (this.subFlag) {
            this.subComponent = this.subComponents[0];
            // this.localDataService.sendToChildUser(this.users[id]);
            this.userService.setDetail(this.users[id]);
            this.setParam('40%', '20%');
        }
    }
    showAdd(): void {
        if (this.subFlag) {
            this.subComponent = this.subComponents[1];
        }
    }

    setParam(top: string, left: string): void {
        this.subWindow = 'show';
        this.subFlag = false;
        this.subTop = top;
        this.subLeft = left;
    }


    goBack(): void {
        this.location.back();
    }
    @HostListener('document:keydown', ['$event'])
    onKeydownHandler(event: KeyboardEvent): boolean {
        console.log(event);
        let x = event.keyCode;
        if (x === 116 || this.onCtrl && x === 82) {
            return this.refreshNavigation();
        }
        if (x === 224 || x === 17) {
            this.onCtrl = true;
        }

    }

    refreshNavigation(): boolean {
        console.log('KeyDown!');
        this.router.navigate(['/admin']);
        return false;
    }

    @HostListener('document:keyup', ['$event'])
    onKeyupHandler(event: KeyboardEvent): void {
        const x = event.keyCode;
        this.onCtrl = (x === 224 || x === 17) ? false : false;
    }
}
