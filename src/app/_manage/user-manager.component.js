"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var common_1 = require("@angular/common");
require("rxjs/add/operator/switchMap");
// Import Component
var index_1 = require("./index");
// Import Service
var index_2 = require("../service/index");
// Import Local Share Service
var local_data_service_1 = require("../service/local-data.service");
var subjects_service_1 = require("../service/subjects.service");
// Import Animation Component
var slide_in_out_animation_1 = require("../lib/animation/slide-in-out.animation");
var slide_content_animation_1 = require("../lib/animation/slide-content.animation");
var UserManagerComponent = (function () {
    function UserManagerComponent(router, location, localDataService, subjectsService, userService) {
        this.router = router;
        this.location = location;
        this.localDataService = localDataService;
        this.subjectsService = subjectsService;
        this.userService = userService;
        this.users = [];
        this.onCtrl = false;
        this.searchOn = true;
        this.searchType = 'none';
        this.subWindow = 'void';
        this.subFlag = true;
        this.subTop = '0px';
        this.subLeft = '0px';
        this.subComponents = [
            index_1.UserDetailComponent,
            index_1.UserAddComponent
        ];
    }
    UserManagerComponent.prototype.ngOnInit = function () {
        this.getUser();
    };
    UserManagerComponent.prototype.getChMsg = function () {
        var _this = this;
        this.localDataService.toParentData$.subscribe(function (msg) {
            if (_this.searchType === 'user') {
                console.log(msg.id);
                _this.users[msg.id] = msg;
            }
            // this.closeSearch();
        });
    };
    UserManagerComponent.prototype.closeSubConponent = function () {
        this.subWindow = 'hide';
        this.subFlag = true;
        this.subTop = '';
        this.subLeft = '';
    };
    UserManagerComponent.prototype.getUser = function () {
        var _this = this;
        this.userService.getAll()
            .then(function (users) { return _this.users = users; });
    };
    UserManagerComponent.prototype.showDetail = function (id) {
        if (this.subFlag) {
            this.subComponent = this.subComponents[0];
            // this.localDataService.sendToChildUser(this.users[id]);
            this.userService.setDetail(this.users[id]);
            this.setParam('40%', '20%');
        }
    };
    UserManagerComponent.prototype.showAdd = function () {
        if (this.subFlag) {
            this.subComponent = this.subComponents[1];
        }
    };
    UserManagerComponent.prototype.setParam = function (top, left) {
        this.subWindow = 'show';
        this.subFlag = false;
        this.subTop = top;
        this.subLeft = left;
    };
    UserManagerComponent.prototype.goBack = function () {
        this.location.back();
    };
    UserManagerComponent.prototype.onKeydownHandler = function (event) {
        console.log(event);
        var x = event.keyCode;
        if (x === 116 || this.onCtrl && x === 82) {
            return this.refreshNavigation();
        }
        if (x === 224 || x === 17) {
            this.onCtrl = true;
        }
    };
    UserManagerComponent.prototype.refreshNavigation = function () {
        console.log('KeyDown!');
        this.router.navigate(['/admin']);
        return false;
    };
    UserManagerComponent.prototype.onKeyupHandler = function (event) {
        var x = event.keyCode;
        this.onCtrl = (x === 224 || x === 17) ? false : false;
    };
    return UserManagerComponent;
}());
__decorate([
    core_1.HostListener('document:keydown', ['$event']),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [KeyboardEvent]),
    __metadata("design:returntype", Boolean)
], UserManagerComponent.prototype, "onKeydownHandler", null);
__decorate([
    core_1.HostListener('document:keyup', ['$event']),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [KeyboardEvent]),
    __metadata("design:returntype", void 0)
], UserManagerComponent.prototype, "onKeyupHandler", null);
UserManagerComponent = __decorate([
    core_1.Component({
        selector: 'user-manager',
        templateUrl: './user-manager.component.html',
        providers: [local_data_service_1.LocalDataService],
        animations: [slide_in_out_animation_1.slideInOutAnimation, slide_content_animation_1.slideContentAnimation],
        host: { '[@slideInOutAnimation]': '' }
    }),
    __metadata("design:paramtypes", [router_1.Router,
        common_1.Location,
        local_data_service_1.LocalDataService,
        subjects_service_1.SubjectsService,
        index_2.UserService])
], UserManagerComponent);
exports.UserManagerComponent = UserManagerComponent;
//# sourceMappingURL=user-manager.component.js.map