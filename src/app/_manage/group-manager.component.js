"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
require("rxjs/add/operator/switchMap");
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var common_1 = require("@angular/common");
// Import Service
// import { Group, GroupService } from '../service/index';
// Import Local Share Service
var local_data_service_1 = require("../service/local-data.service");
var subjects_service_1 = require("../service/subjects.service");
// Import Animation Component
var slide_in_out_animation_1 = require("../lib/animation/slide-in-out.animation");
var GroupManagerComponent = (function () {
    function GroupManagerComponent(router, location, localDataService, subjectsService) {
        this.router = router;
        this.location = location;
        this.localDataService = localDataService;
        this.subjectsService = subjectsService;
        // groups: Group[] = [];
        this.onCtrl = false;
    }
    GroupManagerComponent.prototype.ngOnInit = function () {
    };
    GroupManagerComponent.prototype.goBack = function () {
        this.location.back();
    };
    GroupManagerComponent.prototype.onKeydownHandler = function (event) {
        console.log(event);
        var x = event.keyCode;
        if (x === 116 || this.onCtrl && x === 82) {
            return this.refreshNavigation();
        }
        if (x === 224 || x === 17) {
            this.onCtrl = true;
        }
    };
    GroupManagerComponent.prototype.refreshNavigation = function () {
        console.log('KeyDown!');
        this.router.navigate(['/arousal']);
        return false;
    };
    GroupManagerComponent.prototype.onKeyupHandler = function (event) {
        var x = event.keyCode;
        this.onCtrl = (x === 224 || x === 17) ? false : false;
    };
    return GroupManagerComponent;
}());
__decorate([
    core_1.HostListener('document:keydown', ['$event']),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [KeyboardEvent]),
    __metadata("design:returntype", Boolean)
], GroupManagerComponent.prototype, "onKeydownHandler", null);
__decorate([
    core_1.HostListener('document:keyup', ['$event']),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [KeyboardEvent]),
    __metadata("design:returntype", void 0)
], GroupManagerComponent.prototype, "onKeyupHandler", null);
GroupManagerComponent = __decorate([
    core_1.Component({
        selector: 'group-manager',
        templateUrl: './group-manager.component.html',
        animations: [slide_in_out_animation_1.slideInOutAnimation],
        host: { '[@slideInOutAnimation]': '' }
    }),
    __metadata("design:paramtypes", [router_1.Router,
        common_1.Location,
        local_data_service_1.LocalDataService,
        subjects_service_1.SubjectsService])
], GroupManagerComponent);
exports.GroupManagerComponent = GroupManagerComponent;
//# sourceMappingURL=group-manager.component.js.map