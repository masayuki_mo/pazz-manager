"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
// Import Main Component
var skill_search_component_1 = require("../lib/search/skill-search.component");
// Import Service
var index_1 = require("../service/index");
// Import TherdParty Service
var index_2 = require("../lib_service/index");
// Import Local Share Service
var local_data_service_1 = require("../service/local-data.service");
var subjects_service_1 = require("../service/subjects.service");
// Import Animation
var slide_content_animation_1 = require("../lib/animation/slide-content.animation");
var SkillComponent = (function () {
    function SkillComponent(router, skillService, localdataService, subjectsService, csvmakerService) {
        this.router = router;
        this.skillService = skillService;
        this.localdataService = localdataService;
        this.subjectsService = subjectsService;
        this.csvmakerService = csvmakerService;
        this.navigate = true;
        this.counts = new Array(0);
        this.start = 0;
        this.len = 10;
        this.subFlag = true;
        this.subWindow = 'void';
    }
    SkillComponent.prototype.ngOnInit = function () {
        this.getSkills();
        this.reloadCheck();
        this.getMsg();
    };
    SkillComponent.prototype.pager = function (page) {
        this.start = this.len * page;
    };
    SkillComponent.prototype.getMsg = function () {
        var _this = this;
        this.localdataService.toParentData$.subscribe(function (msg) {
            console.log(msg.Name);
            _this.closeSearch();
            _this.router.navigate(['/skill/detail/', msg.id]);
        });
    };
    SkillComponent.prototype.reloadCheck = function () {
        var _this = this;
        this.subscription = this.subjectsService
            .on('skill-updated')
            .subscribe(function () {
            console.log('Do update Skills');
            _this.getSkills();
        });
    };
    SkillComponent.prototype.getSkills = function () {
        var _this = this;
        this.skillService
            .getSkills()
            .then(function (skills) { return _this.skills = skills; })
            .then(function (skills) { return _this.counts = new Array(Math.ceil(skills.length / 10)); });
    };
    SkillComponent.prototype.delete = function (skill) {
        var _this = this;
        this.skillService
            .delete(skill.id)
            .then(function () {
            _this.skills = _this.skills
                .filter(function (_skill) { return _skill !== skill; });
            if (_this.selectedSkill === skill) {
                _this.selectedSkill = null;
            }
        });
    };
    SkillComponent.prototype.showSearch = function () {
        this.searchContent = skill_search_component_1.SkillSearchComponent;
        this.subFlag = false;
        this.subWindow = 'show';
    };
    SkillComponent.prototype.closeSearch = function () {
        this.subFlag = true;
        this.subWindow = 'hide';
    };
    SkillComponent.prototype.gotoDetail = function () {
        this.router.navigate(['./detail-s', this.selectedSkill.id]);
    };
    SkillComponent.prototype.onSelect = function (skill) {
        this.selectedSkill = skill;
        console.log(this.selectedSkill);
    };
    SkillComponent.prototype.getcsvFile = function () {
        this.csvmakerService.setCsvName('skill_list');
        var data = this.csvmakerService.convObjectToCSV(this.skills);
        this.csvmakerService.getCsv(data);
    };
    return SkillComponent;
}());
SkillComponent = __decorate([
    core_1.Component({
        selector: 'pazz-skills',
        templateUrl: './skill.component.html',
        providers: [local_data_service_1.LocalDataService],
        animations: [slide_content_animation_1.slideContentAnimation]
    }),
    __metadata("design:paramtypes", [router_1.Router,
        index_1.SkillService,
        local_data_service_1.LocalDataService,
        subjects_service_1.SubjectsService,
        index_2.CsvMakerService])
], SkillComponent);
exports.SkillComponent = SkillComponent;
//# sourceMappingURL=skill.component.js.map