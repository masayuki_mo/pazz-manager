"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var router_1 = require("@angular/router");
// Import Service
var index_1 = require("../service/index");
// Import Local Share Service
var local_data_service_1 = require("../service/local-data.service");
var subjects_service_1 = require("../service/subjects.service");
// Import Animation Component
var slide_in_out_animation_1 = require("../lib/animation/slide-in-out.animation");
var SkillAddComponent = (function () {
    function SkillAddComponent(router, skillService, location, localDataService, subjectsService) {
        this.router = router;
        this.skillService = skillService;
        this.location = location;
        this.localDataService = localDataService;
        this.subjectsService = subjectsService;
        this.skill = new index_1.Skill();
        this.onCtrl = false;
    }
    SkillAddComponent.prototype.add = function () {
        var _this = this;
        if (!this.skill.id && !this.skill.Name) {
            return;
        }
        this.skillService.create(name)
            .then(function () {
            _this.subjectsService.publish('skill-updated');
            // console.log('new skill add success');
            _this.goBack();
        });
    };
    SkillAddComponent.prototype.goBack = function () {
        this.location.back();
    };
    SkillAddComponent.prototype.onKeydownHandler = function (event) {
        console.log(event);
        var x = event.keyCode;
        if (x === 116 || this.onCtrl && x === 82) {
            return this.refreshNavigation();
        }
        if (x === 224 || x === 17) {
            this.onCtrl = true;
        }
    };
    SkillAddComponent.prototype.refreshNavigation = function () {
        console.log('KeyDown!');
        this.router.navigate(['/arousal']);
        return false;
    };
    SkillAddComponent.prototype.onKeyupHandler = function (event) {
        var x = event.keyCode;
        this.onCtrl = (x === 224 || x === 17) ? false : false;
    };
    return SkillAddComponent;
}());
__decorate([
    core_1.HostListener('document:keydown', ['$event']),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [KeyboardEvent]),
    __metadata("design:returntype", Boolean)
], SkillAddComponent.prototype, "onKeydownHandler", null);
__decorate([
    core_1.HostListener('document:keyup', ['$event']),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [KeyboardEvent]),
    __metadata("design:returntype", void 0)
], SkillAddComponent.prototype, "onKeyupHandler", null);
SkillAddComponent = __decorate([
    core_1.Component({
        selector: 'skill-add',
        templateUrl: './skill-add.component.html',
        animations: [slide_in_out_animation_1.slideInOutAnimation],
        host: { '[@slideInOutAnimation]': '' }
    }),
    __metadata("design:paramtypes", [router_1.Router,
        index_1.SkillService,
        common_1.Location,
        local_data_service_1.LocalDataService,
        subjects_service_1.SubjectsService])
], SkillAddComponent);
exports.SkillAddComponent = SkillAddComponent;
//# sourceMappingURL=skill-add.component.js.map