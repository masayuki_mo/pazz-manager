import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
// Import Main Component
import { SkillSearchComponent } from '../lib/search/skill-search.component';
// Import Service
import { Skill, SkillService } from '../service/index';
// Import TherdParty Service
import { CsvMakerService } from '../lib_service/index';
// Import Local Share Service
import { LocalDataService } from '../service/local-data.service';
import { SubjectsService } from  '../service/subjects.service';
// Import Animation
import { slideContentAnimation } from '../lib/animation/slide-content.animation';

@Component({
    selector: 'pazz-skills',
    templateUrl: './skill.component.html',
    providers: [ LocalDataService ],
    animations: [ slideContentAnimation ]
})

export class SkillComponent implements OnInit {
    navigate: boolean = true;
    skills: Skill[];
    selectedSkill: Skill;

    counts: number[] = new Array(0);

    start = 0;
    len = 10;

    subFlag: boolean = true;
    subWindow: string = 'void';
    searchContent: any;

    subscription: Subscription;

    constructor(
        private router: Router,
        private skillService: SkillService,
        private localdataService: LocalDataService,
        private subjectsService: SubjectsService,
        private csvmakerService: CsvMakerService
    ) {}


    ngOnInit(): void {
        this.getSkills();
        this.reloadCheck();
        this.getMsg();
    }

    pager(page: number) {
        this.start = this.len * page;
    }
    getMsg(): any {
        this.localdataService.toParentData$.subscribe(
            msg => {
                console.log(msg.Name);
                this.closeSearch();
                this.router.navigate(['/skill/detail/', msg.id]);
            }
        );
    }
    reloadCheck(): void {
        this.subscription = this.subjectsService
            .on('skill-updated')
            .subscribe(() => {
                console.log('Do update Skills');
                this.getSkills();
            });
    }
    getSkills(): void {
        this.skillService
            .getSkills()
            .then((skills: Skill[]) => this.skills = skills)
            .then((skills: Skill[]) => this.counts = new Array(Math.ceil(skills.length / 10)) );
    }

    delete(skill: Skill): void {
        this.skillService
            .delete(skill.id)
            .then(() => {
                this.skills = this.skills
                    .filter((_skill: Skill) => _skill !== skill);
                if (this.selectedSkill === skill) {
                    this.selectedSkill = null;
                }
            });
    }
    showSearch(): void {
        this.searchContent = SkillSearchComponent;
        this.subFlag = false;
        this.subWindow = 'show';
    }
    closeSearch(): void {
        this.subFlag = true;
        this.subWindow = 'hide';
    }
    gotoDetail(): void {
        this.router.navigate(['./detail-s', this.selectedSkill.id]);
    }

    onSelect(skill: Skill): void {
        this.selectedSkill = skill;
        console.log(this.selectedSkill);
    }

    getcsvFile(): void {
        this.csvmakerService.setCsvName('skill_list');
        let data = this.csvmakerService.convObjectToCSV(this.skills);
        this.csvmakerService.getCsv(data);
    }
}
