"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
require("rxjs/add/operator/toPromise");
require("rxjs/add/operator/map");
var ArousalService = (function () {
    function ArousalService(http) {
        this.http = http;
        this.serverUrl = 'server/index.php';
        this.headerJson = new http_1.Headers({ 'Content-Type': 'application/json' });
        this.job = '_a';
        this.arousals = new Array();
        this.responseStatus = [];
    }
    ArousalService.prototype.getArousals = function () {
        var _this = this;
        if (this.arousals.length > 1) {
            return new Promise(function (resolve, reject) {
                resolve(_this.arousals);
            });
        }
        else {
            var data = {};
            data['job'] = 'get' + this.job;
            data['id'] = 'all';
            return this.http
                .post(this.serverUrl, JSON.stringify(data), { headers: this.headerJson })
                .toPromise()
                .then(function (response) {
                _this.arousals = response.json();
                return response.json();
            })
                .catch(this.handleError);
        }
    };
    ArousalService.prototype.getArousal = function (id) {
        var _this = this;
        if (this.arousals.length > 1) {
            return new Promise(function (resolve, reject) {
                resolve(_this.arousals[id]);
            });
        }
        else {
            var data = {};
            data['job'] = 'get' + this.job;
            data['id'] = "" + id;
            return this.http
                .post(this.serverUrl, JSON.stringify(data), { headers: this.headerJson })
                .toPromise()
                .then(function (response) { return response.json(); })
                .catch(this.handleError);
        }
    };
    ArousalService.prototype.create = function (arousal) {
        var _this = this;
        arousal['job'] = 'add' + this.job;
        return this.http
            .post(this.serverUrl, JSON.stringify(arousal), { headers: this.headerJson })
            .toPromise()
            .then(function (response) {
            _this.responseStatus = response.json();
            _this.arousals.push(arousal);
        })
            .catch(this.handleError);
    };
    ArousalService.prototype.update = function (arousal) {
        var _this = this;
        var update = arousal;
        update['job'] = 'update' + this.job;
        return this.http
            .post(this.serverUrl, JSON.stringify(update), { headers: this.headerJson })
            .toPromise()
            .then(function (response) {
            _this.arousals[arousal.id] = arousal;
            _this.responseStatus = response.json();
            for (var key in arousal) {
                if (arousal.hasOwnProperty(key)) {
                    _this.arousals[(arousal.id)][key] = arousal[key];
                }
            }
            _this.arousals[(arousal.id)] = arousal;
        })
            .catch(this.handleError);
    };
    ArousalService.prototype.delete = function (id) {
        var _this = this;
        return this.http
            .delete(this.serverUrl, { headers: this.headerJson })
            .toPromise()
            .then(function () {
            _this.arousals.splice(id, 1);
        })
            .catch(this.handleError);
    };
    ArousalService.prototype.handleError = function (error) {
        console.error('An error occured', error);
        return Promise.reject(error.message || error);
    };
    return ArousalService;
}());
ArousalService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], ArousalService);
exports.ArousalService = ArousalService;
//# sourceMappingURL=arousal.service.js.map