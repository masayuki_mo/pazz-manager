export class Leader {
    id:             number;
    Name:           string;
    Type:           string;
    Attribute:      string;
    MType:          string;
    Rate:           string;
    Program:        number;
    Propatie:       string;
    PType:          string;
    Description:    string;
    etc3:           string;
    etc4:           string;
}
