import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class LocalDataService {

    constructor() {}

    private toParentDataSouce = new Subject<any>();
    private toChildDataMonster = new Subject<any>();
    private toChildDataSkill = new Subject<any>();
    private toChildDataLeader = new Subject<any>();
    private toChildDataArousal = new Subject<any>();
    private toChildDataType = new Subject<any>();
    private toChildDataAttribute = new Subject<any>();
    private toChildDataUser = new Subject<any>();


    public toParentData$ = this.toParentDataSouce.asObservable();
    public toChildDataMonster$ = this.toChildDataMonster.asObservable();
    public toChildDataLeader$ = this.toChildDataLeader.asObservable();
    public toChildDataSkill$ = this.toChildDataSkill.asObservable();
    public toChildDataArousal$ = this.toChildDataArousal.asObservable();
    public toChildDataType$ = this.toChildDataType.asObservable();
    public toChildDataAttribute$ = this.toChildDataAttribute.asObservable();
    public toChildDataUser$ = this.toChildDataUser.asObservable();

    sendToParent(msg: any) {
        this.toParentDataSouce.next(msg);
    }

    sendToChildMonster(monster: any) {
        this.toChildDataMonster.next(monster);
    }
    sendToChildSkill(skill: any) {
        this.toChildDataSkill.next(skill);
    }
    sendToChildLeader(leader: any) {
        this.toChildDataLeader.next(leader);
    }
    sendToChildArousal(arousal: any) {
        this.toChildDataArousal.next(arousal);
    }
    sendToChildType(type: any) {
        this.toChildDataType.next(type);
    }
    sendToChildAttribute(type: any) {
        this.toChildDataAttribute.next(type);
    }
    sendToChildUser(type: any) {
        this.toChildDataUser.next(type);
    }
}
