import { Injectable } from '@angular/core';
import { Headers, Http, Response} from '@angular/http';
// import { Observable } from 'rxjs';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import { Leader } from './leader';

@Injectable()
export class LeaderService {

    private serverUrl: string = 'server/index.php';
    private headerJson: Headers = new Headers({'Content-Type': 'application/json'});
    private job: string = '_l';
    private leaders: Leader[] = new Array();
    private responseStatus: any[] = [];

    constructor(private http: Http) {}

    getLeaders(): Promise<Leader[]> {
        if (this.leaders.length > 1) {
            return new Promise((resolve, reject) => {
                    resolve(this.leaders);
                }
            );
        } else {
            let data = {};
            data['job'] = 'get' + this.job;
            data['id'] = 'all';

            return this.http
                .post(this.serverUrl,
                     JSON.stringify(data),
                    { headers: this.headerJson } )
                .toPromise()
                .then(response => {
                    this.leaders = response.json();
                    return response.json() as Leader[];
                })
                .catch(this.handleError);
        }
    }

    getLeader(id: number): Promise<Leader> {
        if (this.leaders.length > 1) {
            return new Promise((resolve, reject) => {
                    resolve(this.leaders[id]);
                }
            );
        } else {
            let data = {};
            data['job'] = 'get' + this.job;
            data['id'] = `${id}`;

            return this.http
                .post(this.serverUrl,
                    JSON.stringify(data),
                    { headers: this.headerJson } )
                .toPromise()
                .then((response: Response) => response.json() as Leader)
                .catch(this.handleError);
        }
    }
    create(leader: Leader): Promise<Leader> {

        leader['job'] = 'add' + +this.job;

        return this.http
            .post(this.serverUrl,
                JSON.stringify(leader),
                { headers: this.headerJson } )
            .toPromise()
            .then((response: Response) => {
                this.responseStatus = response.json();
                this.leaders[leader.id] = leader;
            })
            .catch(this.handleError);
    }

    update(leader: Leader): Promise<Leader> {

        leader['job'] = 'update' + this.job;
        console.log(leader);

        return this.http
            .post(this.serverUrl,
                JSON.stringify(leader),
                { headers: this.headerJson } )
            .toPromise()
            .then((response: Response) => {
                this.responseStatus = response.json();

                for (let key in leader) {
                    if (leader.hasOwnProperty(key)) {
                        this.leaders[leader.id][key] = leader[key];
                    }
                }
                this.leaders[leader.id] = leader;
            })
            .catch(this.handleError);
    }

    delete(id: number): Promise<void> {
        return this.http
            .delete(this.serverUrl, {headers: this.headerJson})
            .toPromise()
            .then(() => {
                this.leaders.splice(id, 1);
            })
            .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occured', error);
        return Promise.reject(error.message || error);
    }
}
