import { Injectable } from '@angular/core';
import { Headers, Http, Response} from '@angular/http';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import { Attribute } from './attribute';
@Injectable()

export class AttributeService {

    private serverUrl: string = 'server/index.php';
    private headerJson: Headers = new Headers({'Content-Type': 'application/json'});
    private job: string = '_at';

    private attributes: Attribute[] = new Array();
    private responseStatus: any[] = [];

    constructor(private http: Http) {}

    getAttributes(): Promise<Attribute[]> {
        if (this.attributes.length > 1) {
            return new Promise((resolve, reject) => {
                resolve(this.attributes);
            });
        } else {
            let data = {};
            data['job'] = 'get' + this.job;
            data['id'] = 'all';

            return this.http
                .post(this.serverUrl,
                     JSON.stringify(data),
                    { headers: this.headerJson } )
                .toPromise()
                .then(response => {
                    this.attributes = response.json();
                    return response.json() as Attribute[];
                })
                .catch(this.handleError);
        }

    }

    getAttribute(id: number): Promise<Attribute> {
        if (this.attributes.length > 1) {
            return new Promise((resolve, reject) => {
                resolve(this.attributes[id]);
            });
        } else {
            let data = {};
            data['job'] = 'get' + this.job;
            data['id'] = `${id}`;

            return this.http
                .post(this.serverUrl,
                    JSON.stringify(data),
                    { headers: this.headerJson } )
                .toPromise()
                .then((response: Response) => response.json() as Attribute)
                .catch(this.handleError);
        }
    }
    create(attribute: Attribute): Promise<Attribute> {

        attribute['job'] = 'add' + this.job;

        return this.http
            .post(this.serverUrl,
                JSON.stringify(attribute),
                { headers: this.headerJson } )
            .toPromise()
            .then((response: Response) => {
                this.responseStatus = response.json();
                this.attributes[attribute.id] = attribute;
            })
            .catch(this.handleError);
    }

    update(attribute: Attribute): Promise<Attribute> {

        const update: Attribute = attribute;
        update['job'] = 'update' + this.job;

        return this.http
            .post(this.serverUrl,
                JSON.stringify(update),
                { headers: this.headerJson } )
            .toPromise()
            .then((response: Response) => {

                this.responseStatus = response.json();

                for (let key in attribute) {
                    if (attribute.hasOwnProperty(key)) {
                        this.attributes[(attribute.id - 1)][key] = attribute[key];
                    }
                }
                this.attributes[attribute.id] = attribute;
            })
            .catch(this.handleError);
    }

    delete(id: number): Promise<void> {
        return this.http
            .delete(this.serverUrl, {headers: this.headerJson})
            .toPromise()
            .then(() => {
                this.attributes.splice(id, 1);
            })
            .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occured', error);
        return Promise.reject(error.message || error);
    }
}
