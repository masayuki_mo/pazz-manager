"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
// import { Observable } from 'rxjs';
require("rxjs/add/operator/toPromise");
require("rxjs/add/operator/map");
var MonsterService = (function () {
    function MonsterService(http) {
        this.http = http;
        this.debug = true;
        this.serverUrl = 'server/index.php';
        this.headerJson = new http_1.Headers({ 'Content-Type': 'application/json' });
        this.job = '_m';
        this.monsters = new Array();
        this.responseStatus = [];
    }
    MonsterService.prototype.getMonsters = function () {
        var _this = this;
        if (this.monsters.length > 1) {
            return new Promise(function (resolve, reject) {
                resolve(_this.monsters);
            });
        }
        else {
            var data = {};
            data['job'] = 'get' + this.job;
            data['id'] = 'all';
            return this.http
                .post(this.serverUrl, JSON.stringify(data), { headers: this.headerJson })
                .toPromise()
                .then(function (response) {
                _this.monsters = response.json();
                return response.json();
            })
                .catch(this.handleError);
        }
    };
    MonsterService.prototype.getMonster = function (id) {
        var _this = this;
        if (this.monsters.length > 1) {
            return new Promise(function (resolve, reject) {
                resolve(_this.monsters[id]);
            });
        }
        else {
            var data = {};
            data['job'] = 'get' + this.job;
            data['id'] = "" + id;
            return this.http
                .post(this.serverUrl, JSON.stringify(data), { headers: this.headerJson })
                .toPromise()
                .then(function (response) { return response.json(); })
                .catch(this.handleError);
        }
    };
    MonsterService.prototype.create = function (monster) {
        var _this = this;
        var addData = monster;
        addData['job'] = 'add' + this.job;
        return this.http
            .post(this.serverUrl, JSON.stringify(addData), { headers: this.headerJson })
            .toPromise()
            .then(function (response) {
            _this.responseStatus = response.json();
            _this.monsters[(monster.id - 1)] = monster;
        })
            .catch(this.handleError);
    };
    MonsterService.prototype.update = function (monster) {
        var _this = this;
        var updata = monster;
        updata['job'] = 'update' + this.job;
        return this.http
            .post(this.serverUrl, JSON.stringify(updata), { headers: this.headerJson })
            .toPromise()
            .then(function (response) {
            _this.responseStatus = response.json();
            _this.pullConsole('Update Monster :' + _this.monsters[(monster.id - 1)].Name);
            for (var key in monster) {
                if (monster.hasOwnProperty(key)) {
                    _this.monsters[(monster.id - 1)][key] = monster[key];
                }
            }
            _this.monsters[(monster.id - 1)] = monster;
        })
            .catch(this.handleError);
    };
    MonsterService.prototype.delete = function (id) {
        var _this = this;
        return this.http
            .delete(this.serverUrl, { headers: this.headerJson })
            .toPromise()
            .then(function (response) { return _this.responseStatus = response.json(); })
            .catch(this.handleError);
    };
    MonsterService.prototype.handleError = function (error) {
        console.error('An error occured', error);
        return Promise.reject(error.message || error);
    };
    MonsterService.prototype.pullConsole = function (mess) {
        if (this.debug) {
            console.log(mess);
        }
    };
    return MonsterService;
}());
MonsterService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], MonsterService);
exports.MonsterService = MonsterService;
//# sourceMappingURL=monster.service.js.map