"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var Subject_1 = require("rxjs/Subject");
var LocalDataService = (function () {
    function LocalDataService() {
        this.toParentDataSouce = new Subject_1.Subject();
        this.toChildDataMonster = new Subject_1.Subject();
        this.toChildDataSkill = new Subject_1.Subject();
        this.toChildDataLeader = new Subject_1.Subject();
        this.toChildDataArousal = new Subject_1.Subject();
        this.toChildDataType = new Subject_1.Subject();
        this.toChildDataAttribute = new Subject_1.Subject();
        this.toChildDataUser = new Subject_1.Subject();
        this.toParentData$ = this.toParentDataSouce.asObservable();
        this.toChildDataMonster$ = this.toChildDataMonster.asObservable();
        this.toChildDataLeader$ = this.toChildDataLeader.asObservable();
        this.toChildDataSkill$ = this.toChildDataSkill.asObservable();
        this.toChildDataArousal$ = this.toChildDataArousal.asObservable();
        this.toChildDataType$ = this.toChildDataType.asObservable();
        this.toChildDataAttribute$ = this.toChildDataAttribute.asObservable();
        this.toChildDataUser$ = this.toChildDataUser.asObservable();
    }
    LocalDataService.prototype.sendToParent = function (msg) {
        this.toParentDataSouce.next(msg);
    };
    LocalDataService.prototype.sendToChildMonster = function (monster) {
        this.toChildDataMonster.next(monster);
    };
    LocalDataService.prototype.sendToChildSkill = function (skill) {
        this.toChildDataSkill.next(skill);
    };
    LocalDataService.prototype.sendToChildLeader = function (leader) {
        this.toChildDataLeader.next(leader);
    };
    LocalDataService.prototype.sendToChildArousal = function (arousal) {
        this.toChildDataArousal.next(arousal);
    };
    LocalDataService.prototype.sendToChildType = function (type) {
        this.toChildDataType.next(type);
    };
    LocalDataService.prototype.sendToChildAttribute = function (type) {
        this.toChildDataAttribute.next(type);
    };
    LocalDataService.prototype.sendToChildUser = function (type) {
        this.toChildDataUser.next(type);
    };
    return LocalDataService;
}());
LocalDataService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [])
], LocalDataService);
exports.LocalDataService = LocalDataService;
//# sourceMappingURL=local-data.service.js.map