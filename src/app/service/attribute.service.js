"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
require("rxjs/add/operator/toPromise");
require("rxjs/add/operator/map");
var AttributeService = (function () {
    function AttributeService(http) {
        this.http = http;
        this.serverUrl = 'server/index.php';
        this.headerJson = new http_1.Headers({ 'Content-Type': 'application/json' });
        this.job = '_at';
        this.attributes = new Array();
        this.responseStatus = [];
    }
    AttributeService.prototype.getAttributes = function () {
        var _this = this;
        if (this.attributes.length > 1) {
            return new Promise(function (resolve, reject) {
                resolve(_this.attributes);
            });
        }
        else {
            var data = {};
            data['job'] = 'get' + this.job;
            data['id'] = 'all';
            return this.http
                .post(this.serverUrl, JSON.stringify(data), { headers: this.headerJson })
                .toPromise()
                .then(function (response) {
                _this.attributes = response.json();
                return response.json();
            })
                .catch(this.handleError);
        }
    };
    AttributeService.prototype.getAttribute = function (id) {
        var _this = this;
        if (this.attributes.length > 1) {
            return new Promise(function (resolve, reject) {
                resolve(_this.attributes[id]);
            });
        }
        else {
            var data = {};
            data['job'] = 'get' + this.job;
            data['id'] = "" + id;
            return this.http
                .post(this.serverUrl, JSON.stringify(data), { headers: this.headerJson })
                .toPromise()
                .then(function (response) { return response.json(); })
                .catch(this.handleError);
        }
    };
    AttributeService.prototype.create = function (attribute) {
        var _this = this;
        attribute['job'] = 'add' + this.job;
        return this.http
            .post(this.serverUrl, JSON.stringify(attribute), { headers: this.headerJson })
            .toPromise()
            .then(function (response) {
            _this.responseStatus = response.json();
            _this.attributes[attribute.id] = attribute;
        })
            .catch(this.handleError);
    };
    AttributeService.prototype.update = function (attribute) {
        var _this = this;
        var update = attribute;
        update['job'] = 'update' + this.job;
        return this.http
            .post(this.serverUrl, JSON.stringify(update), { headers: this.headerJson })
            .toPromise()
            .then(function (response) {
            _this.responseStatus = response.json();
            for (var key in attribute) {
                if (attribute.hasOwnProperty(key)) {
                    _this.attributes[(attribute.id - 1)][key] = attribute[key];
                }
            }
            _this.attributes[attribute.id] = attribute;
        })
            .catch(this.handleError);
    };
    AttributeService.prototype.delete = function (id) {
        var _this = this;
        return this.http
            .delete(this.serverUrl, { headers: this.headerJson })
            .toPromise()
            .then(function () {
            _this.attributes.splice(id, 1);
        })
            .catch(this.handleError);
    };
    AttributeService.prototype.handleError = function (error) {
        console.error('An error occured', error);
        return Promise.reject(error.message || error);
    };
    return AttributeService;
}());
AttributeService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], AttributeService);
exports.AttributeService = AttributeService;
//# sourceMappingURL=attribute.service.js.map