import { NgModule } from '@angular/core';
import { APP_BASE_HREF } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule, JsonpModule } from '@angular/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// import Route Component
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
// import main component
import { NavigateComponent } from './navigate/navigate.component';
import { MonsterComponent, MonsterAddComponent, MonsterDetailComponent} from './monster/index';
import { LeaderComponent, LeaderAddComponent, LeaderDetailComponent } from './leader/index';
import { SkillComponent, SkillAddComponent, SkillDetailComponent } from './skill/index';
import { ArousalComponent, ArousalAddComponent, ArousalDetailComponent } from './arousal/index';
import { DashboardComponent } from './dashboard/index';
import { ManagerComponent, UserManagerComponent, GroupManagerComponent } from './_manage/index';
import { UserAddComponent, UserDetailComponent } from './_manage/index';
// import sub component
import { MonsterSearchComponent, SkillSearchComponent, LeaderSearchComponent } from './lib/search/index';
import { TypeSelectComponent, AttributeSelectComponent, ArousalSelectComponent } from './lib/select/index';
import { ClockSelectComponent } from './lib/select/index';
import { PdfPrintComponent } from './lib/index';
// import service
import { MonsterService, SkillService, LeaderService } from './service/index';
import { ArousalService, AttributeService, MtypeService } from './service/index';
import { SubjectsService } from './service/index';
// import therdpaty service
import { CsvMakerService, PdfMakerService } from './lib_service/index';
import { ListLayoutService, RecruiteLayoutService } from './lib_service/index';
// Import secur Service
import { AlertService, AuthenticationService, UserService  } from './service/index';
import { LoginComponent } from './_gard/login.component';
import { AuthGuard } from './_gard/auth.gard';
import { AlertComponent } from './_gard/alert.component';

// Import Sub Function
import { ImageLoaderComponent, ImageResizeService, ImageSaveService } from './lib/image_module/index';
import { ImagePazzComponent } from './lib/image_module/index';

@NgModule({
    imports:    [
        BrowserModule,
        FormsModule,
        HttpModule,
        JsonpModule,
        AppRoutingModule,
        BrowserAnimationsModule
    ],
    declarations: [
        AppComponent,
        NavigateComponent,
        MonsterDetailComponent, MonsterComponent, MonsterAddComponent,
        SkillDetailComponent, SkillComponent, SkillAddComponent,
        LeaderDetailComponent, LeaderComponent, LeaderAddComponent,
        ArousalDetailComponent, ArousalComponent, ArousalAddComponent,
        DashboardComponent,
        ManagerComponent, UserManagerComponent, GroupManagerComponent,
        UserAddComponent, UserDetailComponent,
        MonsterSearchComponent, SkillSearchComponent, LeaderSearchComponent,
        TypeSelectComponent, AttributeSelectComponent, ArousalSelectComponent,
        ClockSelectComponent,
        PdfPrintComponent, ImageLoaderComponent, ImagePazzComponent,
        AlertComponent, LoginComponent
    ],
    entryComponents: [
        MonsterSearchComponent,
        SkillSearchComponent,
        LeaderSearchComponent,
        TypeSelectComponent,
        AttributeSelectComponent,
        ArousalSelectComponent,
        ClockSelectComponent,
        UserAddComponent,
        UserDetailComponent,
        PdfPrintComponent,
        ImageLoaderComponent,
        ImagePazzComponent,
    ],
    bootstrap: [ AppComponent ],
    providers: [
        {
            provide: APP_BASE_HREF, useValue: '/Angular4/pazz-manager/dist'
        },
        HttpModule,
        JsonpModule,
        MonsterService, SkillService, LeaderService,
        ArousalService, AttributeService, MtypeService,
        SubjectsService,
        AuthGuard, AuthenticationService,
        AlertService,
        UserService,
        CsvMakerService, PdfMakerService,
        ListLayoutService, RecruiteLayoutService,
        ImageResizeService, ImageSaveService
    ]
})
export class AppModule { }
