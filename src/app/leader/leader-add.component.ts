import { Component, HostListener } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
// Import Service
import { Leader, LeaderService } from '../service/index';
// Import Local Share Service
import { LocalDataService } from '../service/local-data.service';
import { SubjectsService } from '../service/subjects.service';
// Import Animation Component
import { slideInOutAnimation } from '../lib/animation/slide-in-out.animation';

@Component({
    selector: 'leader-add',
    templateUrl: './leader-add.component.html',
    animations: [slideInOutAnimation],
    host: { '[@slideInOutAnimation]': '' }
})

export class LeaderAddComponent {

    public leader: Leader = new Leader();
    onCtrl: boolean = false;

    constructor(
        private router: Router,
        private leaderService: LeaderService,
        private location: Location,
        private localDataService: LocalDataService,
        private subjectsService: SubjectsService
    ) {}

    add(): void {

        if (!this.leader.id && !this.leader.Name) { return; }
        let state = this.leaderService.create(this.leader)
            .then(() => {
                this.subjectsService.publish('leader-updated');
                // console.log('new leader skill add success');
                this.goBack();
            });
        if (state) {

        }
    }

    goBack(): void {
        this.location.back();
    }

    @HostListener('document:keydown', ['$event'])
    onKeydownHandler(event: KeyboardEvent): boolean {
        console.log(event);
        let x = event.keyCode;
        if (x === 116) {
            return  this.refreshNavigation();
        } else if ( this.onCtrl && x === 82) {
            return this.refreshNavigation();
        }

        if (x === 224 || x === 17) {
            this.onCtrl = true;
        }

    }

    refreshNavigation(): boolean {
        console.log('KeyDown!');
        this.router.navigate(['/arousal']);
        return false;
    }

    @HostListener('document:keyup', ['$event'])
    onKeyupHandler(event: KeyboardEvent): void {
        const x = event.keyCode;
        this.onCtrl = (x === 224 || x === 17) ? false : false;
    }
}
