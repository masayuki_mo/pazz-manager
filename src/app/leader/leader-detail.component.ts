import 'rxjs/add/operator/switchMap';
import { Component, HostListener, OnInit} from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';

import { Leader } from '../service/leader';
import { LeaderService } from '../service/leader.service';
// Import Local Share Service
import { LocalDataService } from '../service/local-data.service';
import { SubjectsService } from '../service/subjects.service';
import { slideInOutAnimation } from '../lib/animation/slide-in-out.animation';

@Component({
  selector: 'leader-detail',
  templateUrl: './leader-detail.component.html',
  animations: [slideInOutAnimation],
  providers: [ LocalDataService ],
  host: { '[@slideInOutAnimation]': '' }
})

export class LeaderDetailComponent implements OnInit {

    leader: Leader;
    onCtrl: boolean = false;

    constructor(
        private leaderService: LeaderService,
        private route: ActivatedRoute,
        private router: Router,
        private location: Location,
        private localDataService: LocalDataService,
        private subjectsService: SubjectsService
    ) {}
    ngOnInit(): void {
        this.route.params
        .switchMap((params: Params) => this.leaderService.getLeader(+params['id']))
        .subscribe((leader: Leader) => this.leader = leader);
    }

    save(): void {
        this.leaderService.update(this.leader)
        .then(() => {
            this.subjectsService.publish('leader-updated');
            this.goBack();
        });
    }
    goBack(): void {
        this.location.back();
    }

    @HostListener('document:keydown', ['$event'])
    onKeydownHandler(event: KeyboardEvent): boolean {
        console.log(event);
        let x = event.keyCode;
        if (x === 116) {
            return  this.refreshNavigation();
        } else if ( this.onCtrl && x === 82) {
            return this.refreshNavigation();
        }

        if (x === 224 || x === 17) {
            this.onCtrl = true;
        }

    }

    refreshNavigation(): boolean {
        console.log('KeyDown!');
        this.router.navigate(['/arousal']);
        return false;
    }

    @HostListener('document:keyup', ['$event'])
    onKeyupHandler(event: KeyboardEvent): void {
        const x = event.keyCode;
        this.onCtrl = (x === 224 || x === 17) ? false : false;
    }
}
