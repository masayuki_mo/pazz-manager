import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private router: Router) { }

    canActivate(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot) {

        if (localStorage.getItem('currentUser')) {
            let date = new Date();
            let time = date.getTime();
            const user = JSON.parse(localStorage.getItem('currentUser'));

            console.log(user['time'] + ':::' + ( Math.floor( time / 1000 )));
            if ( user['time'] <= ( Math.floor( time / 1000 ) - 30000) ) {
                this.router.navigate(['/login'], { queryParams: { returnUrl: state.url }});
                return false;
            } else {
                return true;
            }
        }

        // not logged in so redirect to login page with the return url
        this.router.navigate(['/login'], { queryParams: { returnUrl: state.url }});
        return false;
    }
}
