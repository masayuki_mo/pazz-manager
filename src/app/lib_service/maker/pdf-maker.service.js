"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var CustomPDFFonts_1 = require("./CustomPDFFonts");
var PdfMakerService = (function () {
    function PdfMakerService() {
        // フォント設定を行う
        Pdfmake.fonts = {
            'msgothic': {
                normal: 'msgothic.ttf',
                bold: 'msgothic.ttf',
                italics: 'msgothic.ttf',
                bolditalics: 'msgothic.ttf',
            }
        };
        Pdfmake.vfs = CustomPDFFonts_1.CUSTOM_PDF_FONTS;
        Hello();
    }
    // 本家createPdfのラッパー、デフォルトフォントを設定
    PdfMakerService.prototype.createPdf = function (docDefinition) {
        docDefinition.defaultStyle = docDefinition.defaultStyle || {};
        docDefinition.defaultStyle.font = 'msgothic';
        // noinspection TypeScriptUnresolvedFunction
        return Pdfmake.createPdf(docDefinition);
    };
    ;
    // URLの取得にもラップをかける
    PdfMakerService.prototype.createPdfUrl = function (docDefinition) {
        var _this = this;
        // callbackが嫌なのでなんとなくPromiseで実装
        return new Promise(function (resolve) {
            var pdfDocGenerator = _this.createPdf(docDefinition);
            setTimeout(function () {
                pdfDocGenerator.getDataUrl(function (url) {
                    setTimeout(function () {
                        resolve(url);
                    });
                });
            });
        });
    };
    return PdfMakerService;
}());
PdfMakerService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [])
], PdfMakerService);
exports.PdfMakerService = PdfMakerService;
//# sourceMappingURL=pdf-maker.service.js.map