"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var router_1 = require("@angular/router");
var index_1 = require("../../service/index");
var local_data_service_1 = require("../../service/local-data.service");
var ArousalSelectComponent = (function () {
    function ArousalSelectComponent(router, location, localDataService, arousalservice) {
        var _this = this;
        this.router = router;
        this.location = location;
        this.localDataService = localDataService;
        this.arousalservice = arousalservice;
        this.arousal = new index_1.Arousal();
        this.setArousals = new Array(0);
        this.arousalservice
            .getArousals()
            .then(function (arousals) { return _this.arousals = arousals; });
        if (this.localDataService.toChildDataArousal$.toString().length > 1) {
            this.selectedArousal = this.localDataService.toChildDataArousal$.toString();
        }
    }
    ArousalSelectComponent.prototype.complete = function () {
        var last = this.setArousals.pop();
        for (var arousal in this.setArousals) {
            if (this.setArousals.hasOwnProperty(arousal)) {
                if (last === Number(arousal)) {
                    this.selectedArousal = this.selectedArousal + arousal;
                    this.localDataService.sendToParent(this.selectedArousal);
                }
                else {
                    this.selectedArousal = this.selectedArousal + arousal + ':';
                }
            }
        }
    };
    ArousalSelectComponent.prototype.addArousal = function (id) {
        if (this.setArousals.length >= 1) {
            this.setArousals.push(id);
        }
        else {
            this.setArousals = [id];
        }
    };
    ArousalSelectComponent.prototype.delArousal = function (id) {
        console.log(id);
        this.setArousals.splice((id), 1);
    };
    return ArousalSelectComponent;
}());
ArousalSelectComponent = __decorate([
    core_1.Component({
        selector: 'arousal-select',
        templateUrl: './arousal-select.html',
        styleUrls: ['./select.scss']
    }),
    __metadata("design:paramtypes", [router_1.Router,
        common_1.Location,
        local_data_service_1.LocalDataService,
        index_1.ArousalService])
], ArousalSelectComponent);
exports.ArousalSelectComponent = ArousalSelectComponent;
//# sourceMappingURL=arousal-select.component.js.map