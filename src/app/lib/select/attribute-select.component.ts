import { Component } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';

import { Attribute } from '../../service/attribute';
import { AttributeService } from '../../service/attribute.service';
import { LocalDataService } from '../../service/local-data.service';

@Component({
    selector: 'attribute-select',
    templateUrl: './attribute-select.html',
    styleUrls: ['./select.scss']
})


export class AttributeSelectComponent {

    public attribute: Attribute = new Attribute();

    attributes: Attribute[];

    constructor(
        private router: Router,
        private location: Location,
        private localDataService: LocalDataService,
        private attributeservice: AttributeService
    ) {
        this.attributeservice
            .getAttributes()
            .then((attributes: Attribute[]) => this.attributes = attributes);
    }

    selectType(attribute: number): void {
        this.localDataService.sendToParent(this.attributes[attribute]);
    }
}
