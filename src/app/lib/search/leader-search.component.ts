import { Component, HostListener } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';

import { Leader, LeaderService } from '../../service/index';
import { LocalDataService } from '../../service/local-data.service';
@Component({
    selector: 'leader-search',
    templateUrl: './leader-search.component.html',
    styleUrls: ['./search.scss']
})

export class LeaderSearchComponent {

    public leader: Leader = new Leader();

    leaders: Leader[];
    searchLeader: string;
    leaderList: Leader[];
    searchOn: boolean = true;

    onAlt: boolean = false;

    constructor(
        private router: Router,
        private location: Location,
        private localDataService: LocalDataService,
        private leaderservice: LeaderService
    ) {
        this.leaderservice
            .getLeaders()
            .then((leaders: Leader[]) => this.leaders = leaders);
    }

    selectLeader(leader: number): void {
        console.log('Chiled::' + this.leaders[leader].Name);
        this.localDataService.sendToParent(this.leaders[leader]);
    }
    closeLeader(): void {
        this.localDataService.sendToParent('close');
    }
    search(): void {
        if (this.searchOn) {
            this.searchOn = false;
            setTimeout(() => {
                this.doSearch();
                this.searchOn = true;
            }, 500);
        }
    }
    doSearch(): void {
        console.log(this.searchLeader);

        if (this.searchLeader !== undefined) {
            this.leaderList = new Array(0);
            for (const leader in this.leaders) {
                if (this.leaders.hasOwnProperty(leader)) {
                    let searchId: string = this.leaders[leader].id.toString();
                    if (this.leaders[leader].Name.indexOf(this.searchLeader) > 0
                        || searchId.indexOf(this.searchLeader) > 0) {
                            this.leaderList.push(this.leaders[leader]);
                        }
                }
            }
        }
    }
    @HostListener('document:keydown', ['$event'])
    onKeydownHandler(event: KeyboardEvent): boolean {
        console.log(event);
        let x = event.keyCode;
        if (x === 8 || x === 27 || this.onAlt && x === 37) {
            return this.refreshNavigation();
        }
        if (x === 18) {
            this.onAlt = true;
        }

    }

    refreshNavigation(): boolean {
        this.closeLeader();
        return false;
    }

    @HostListener('document:keyup', ['$event'])
    onKeyupHandler(event: KeyboardEvent): void {
        const x = event.keyCode;
        this.onAlt = (x === 18) ? false : false;
    }
}
