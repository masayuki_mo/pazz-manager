"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var router_1 = require("@angular/router");
var index_1 = require("../../service/index");
var local_data_service_1 = require("../../service/local-data.service");
var MonsterSearchComponent = (function () {
    function MonsterSearchComponent(router, location, localDataService, monsterService) {
        var _this = this;
        this.router = router;
        this.location = location;
        this.localDataService = localDataService;
        this.monsterService = monsterService;
        this.monster = new index_1.Monster();
        this.searchOn = true;
        this.monsterService
            .getMonsters()
            .then(function (monsters) { return _this.monsters = monsters; });
    }
    MonsterSearchComponent.prototype.selectMonster = function (monster) {
        var id = monster - 1;
        console.log('Chiled::' + this.monsters[id].Name);
        this.localDataService.sendToParent(this.monsters[id]);
    };
    MonsterSearchComponent.prototype.search = function () {
        var _this = this;
        if (this.searchOn) {
            this.searchOn = false;
            setTimeout(function () {
                _this.doSearch();
                _this.searchOn = true;
            }, 500);
        }
    };
    MonsterSearchComponent.prototype.doSearch = function () {
        console.log(this.searchMonster);
        if (this.searchMonster !== undefined) {
            this.monsterList = new Array(0);
            for (var monster in this.monsters) {
                if (this.monsters.hasOwnProperty(monster)) {
                    var searchId = this.monsters[monster].id.toString();
                    if (this.monsters[monster].Name.indexOf(this.searchMonster) > 0
                        || searchId.indexOf(this.searchMonster) > 0) {
                        this.monsterList.push(this.monsters[monster]);
                    }
                }
            }
        }
    };
    return MonsterSearchComponent;
}());
MonsterSearchComponent = __decorate([
    core_1.Component({
        selector: 'monster-search',
        templateUrl: './monster-search.component.html',
        styleUrls: ['./search.scss']
    }),
    __metadata("design:paramtypes", [router_1.Router,
        common_1.Location,
        local_data_service_1.LocalDataService,
        index_1.MonsterService])
], MonsterSearchComponent);
exports.MonsterSearchComponent = MonsterSearchComponent;
//# sourceMappingURL=monster-search.component.js.map