import { ModuleWithProviders, NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';

// import main component
import { ImageLoaderComponent } from './image-loader.component';
// import sub component
import { ImageResizeService } from './service/image-resize.service';

@NgModule({
    imports:    [
        CommonModule
    ],
    declarations: [
        ImageLoaderComponent
    ],
    providers: [
        ImageResizeService
    ],
    exports: [
        ImageLoaderComponent,
        ImageResizeService
    ]
})
export class ImageModule {
    constructor (@Optional() @SkipSelf() parentModule: ImageModule) {
        if (parentModule) {
          throw new Error(
            'CoreModule is already loaded. Import it in the AppModule only');
        }
      }
 }
