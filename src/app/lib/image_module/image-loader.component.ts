import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';

// Import Shared Service
import { SubjectsService } from '../../service/subjects.service';
// Import Animation
// import { slideContentAnimation } from '../../lib/animation/slide-content.animation';

// Import Lib Service
import { ImageResizeService } from './service/image-resize.service';
import { ImageSaveService } from './service/image-save.service';

@Component({
    selector: 'image-loader',
    templateUrl: './image-loader.component.html',
    styleUrls: ['./loader.scss']
})

export class ImageLoaderComponent {

    reader = [];        // ローカルファイルデータ
    imageFiles = [];    // 画像データ（名前、ファイル形式、base64データ）
    imageHistory = [];  // 画像編集履歴
    fileCount = 0;      // 読み込みファイル数の合計
    loadedCount = 0;    // 読み込んだファイル数

    showUpBase = true;
    showEditBase = false;
    showBox = false;
    catOn = false;
    toolBox = false;
    historyBox = false;
    showReload = false;

    onLoad = false;
    onUp = false;
    onDrag = false;
    onImage = '';

    moveSwitch = false;
    mouseStartingPointX = 0;
    mouseStartingPointY = 0;
    mouseMoveBaseX = 0;
    mouseMoveBaseY = 0;
    mouseMoveX = 0;
    mouseMoveY = 0;

    x = 0;
    y = 0;
    width = 200;
    height = 300;

    editTarget = 0;
    editTargetWidth = 0;
    editTargetHeight = 0;
    editName = '';
    canvasBase: HTMLCanvasElement;
    canvasImage: HTMLImageElement;
    canvasWidth;
    canvasHeight;
    canvasRatio;

    catW = 0;
    catH = 0;
    catA = 0;

    subscription: Subscription;
    constructor(
        private router: Router,
        private subjectsService: SubjectsService,
        private imageResizeService: ImageResizeService,
        private imageSaveService: ImageSaveService
    ) {
        this.reader[0] = new FileReader();
    }

    /****************************************************
     *
     * ドラッグイベント
     *
     ***************************************************/


    /**
     *
     * @param event
     */
    onDragOverHandler(event: DragEvent): void {
        this.onDrag = true;
        event.preventDefault();

    }
    onDragLeaveHandler(event: DragEvent): void {
        this.onDrag = false;
        event.stopPropagation();
    }

    /**
     * ファイルドロップイベント
     * @param event ドラッグされたファイル
     */
    onDropHandler(event: DragEvent): void {
        event.preventDefault();
        this.onDrag = false;
        this.showBox = true;
        // 読み込み中表示
        this.onLoad = true;
        // 各変数初期化
        this.reset('fast');
        if (this.canvasImage) {
            // イベントが残っている場合イベントも初期化
            this.reset('last');
        }
        const files = event.dataTransfer.files;
        this.fileCount = files.length;

        // データタイプの判定 読み込み
        if (!files[0] || files[0].type.indexOf('image/') < 0) {
        } else {

            // ファイルが1つの場合
            if (this.fileCount <= 1) {
                this.reader[0] = new FileReader();
                this.reader[0].onloadend = (e) => {
                    // 読み込み中表示消す
                    const imageType = this.getFileType(e.target.result);
                    const imageName = this.getRandomName();
                    this.onLoad = false;
                    this.imageFiles.push({
                        name: imageName,
                        type: imageType,
                        data: e.target.result});
                    console.log(this.imageFiles);
                    this.switchWindow(e);
                };
                this.reader[0].readAsDataURL(files[0]);

            // ファイルが複数の場合
            } else if (this.fileCount > 1) {
                for (let i = 0; i <= this.fileCount - 1; i++) {
                    this.reader[i] = new FileReader();
                    this.reader[i].onloadend = (e) => {
                        try {
                            const imageType = this.getFileType(e.target.result);
                            const imageName = this.getRandomName();
                            this.imageFiles[i] = {
                                name: imageName,
                                type: imageType,
                                data: this.reader[i].result};
                            this.loadedCount++;
                            if (this.loadedCount === this.fileCount) {
                                // 読み込み中表示消す
                                this.onLoad = false;
                                this.switchWindow(e);
                            }
                        } catch (error) {
                            console.log('image read error' + error);
                        }
                    };
                    this.reader[i].readAsDataURL(files[i]);
                }
            }
        }
        event.stopPropagation();
    }
    /**
     * 読み込んだファイルの表示
     * @param event ドラッグされたファイル
     */
    onSelectHandler(event): void {
        this.onDrag = false;
        this.showBox = true;
        // 読み込み中表示
        this.onLoad = true;
        // 各変数初期化
        this.reset('fast');
        if (this.canvasImage) {
            // イベントが残っている場合イベントも初期化
            this.reset('last');
        }
        const files = event.target.files;
        this.fileCount = files.length;

        // データタイプの判定 読み込み
        if (!files[0] || files[0].type.indexOf('image/') < 0) {
        } else {

            // ファイルが1つの場合
            if (this.fileCount <= 1) {
                this.reader[0] = new FileReader();
                this.reader[0].onloadend = (e) => {
                    // 読み込み中表示消す
                    const imageType = this.getFileType(e.target.result);
                    const imageName = this.getRandomName();
                    this.onLoad = false;
                    this.imageFiles.push({
                        name: imageName,
                        type: imageType,
                        data: e.target.result});
                    console.log(this.imageFiles);
                    this.switchWindow(e);
                };
                this.reader[0].readAsDataURL(files[0]);

            // ファイルが複数の場合
            } else if (this.fileCount > 1) {
                const readImages = [];
                for (const key in files) {
                    if (files.hasOwnProperty(key)) {
                        this.reader[key] = new FileReader();
                        this.reader[key].onloadend = (e) => {
                            const imageType = this.getFileType(e.target.result);
                            const imageName = this.getRandomName();
                            readImages[key] = {
                                name: imageName,
                                type: imageType,
                                data: this.reader[key].result};
                            this.loadedCount++;
                            if (this.loadedCount === this.fileCount) {
                                // 読み込み中表示消す
                                this.onLoad = false;
                                this.imageFiles = readImages;
                                this.switchWindow(e);
                            }
                        };
                        this.reader[key].readAsDataURL(files[key]);
                    }
                }
            }
        }
        event.stopPropagation();
    }

    /************************************************
     *
     * ファイル編集
     *
     ***********************************************/


    imageEditView(image: number): void {
        this.editTarget = image;
        this.editName = this.imageFiles[this.editTarget].name;
        // 表示ボックス切り替え
        this.toolBox = true;
        this.historyBox = true;
        this.catOn = false;

        // canvas要素の取得doctypeを指定しないとエラーになるので注意
        this.canvasBase = <HTMLCanvasElement> document.getElementById('canvas');
        const ctx = this.canvasBase.getContext('2d');
        ctx.imageSmoothingEnabled = true;

        // いきなり画像を縮小するとブロックノイズが発生する対策用
        // 非表示で画像要素を作成し段階的に縮小させる
        // 無くても良い
        const oc = <HTMLCanvasElement> document.createElement('canvas');
        const octx = oc.getContext('2d');
        /**
         * 画像追加処理
         */
        this.canvasImage = new Image();
        this.canvasImage.onload = (e) => {
            // 編集対象画像のサイズを表示
            this.editTargetWidth = this.canvasImage.naturalWidth;
            this.editTargetHeight = this.canvasImage.naturalHeight;

            const ctxw = this.canvasImage.naturalWidth * 0.5;
            const ctxh = this.canvasImage.naturalHeight * 0.5;
            octx.drawImage(this.canvasImage, 0, 0, ctxw, ctxh);
            if ( ctxw >= 600 ) {
                octx.drawImage(this.canvasImage, 0, 0, ctxw * 0.5, ctxh * 0.5);
            }

            this.canvasWidth = 350;
            this.canvasRatio = this.canvasWidth / this.canvasImage.naturalWidth;
            this.canvasHeight = this.canvasImage.naturalHeight * this.canvasRatio;
            this.canvasBase.setAttribute('width', this.canvasWidth.toString());
            this.canvasBase.setAttribute('height', this.canvasHeight.toString());
            ctx.drawImage(this.canvasImage, 0, 0, this.canvasWidth, this.canvasHeight);


            this.setMouseEvent();
        };
        this.canvasImage.setAttribute('class', 'org-image');
        this.canvasImage.src = this.imageFiles[image]['data'];
    }

    /**
     * リサイズ領域表示イベント登録
     *
     */
    setMouseEvent(): void {
        this.canvasBase.addEventListener('mousedown', (e) => {
            this.mouseStartingPointX = e.offsetX;
            this.mouseStartingPointY = e.offsetY;
            this.moveSwitch = true;
            this.catOn = true;
        }, false);
        this.canvasBase.addEventListener('mouseup', (e) => {
            console.log(this.mouseStartingPointX + ':' + this.mouseStartingPointY + ':' + this.mouseMoveX + ':' + this.mouseMoveY);
            this.moveSwitch = false;
        }, false);
        this.canvasBase.addEventListener('mousemove', (e) => {
            if (this.moveSwitch) {
                const ctx = this.canvasBase.getContext('2d');
                ctx.clearRect( 0, 0, this.canvasWidth, this.canvasHeight);
                this.mouseMoveX = -(this.mouseStartingPointX - e.offsetX);
                this.mouseMoveY = -(this.mouseStartingPointY - e.offsetY);
                this.mouseMoveX = (this.mouseMoveX < 0) ? -(this.mouseMoveX) : this.mouseMoveX;
                this.mouseMoveY = (this.mouseMoveY < 0) ? -(this.mouseMoveY) : this.mouseMoveY;
                this.catW = this.mouseMoveX;
                this.catH = this.mouseMoveY;
                ctx.drawImage(this.canvasImage, 0, 0, this.canvasWidth, this.canvasHeight);
                ctx.fillRect(this.mouseStartingPointX, this.mouseStartingPointY, this.mouseMoveX, this.mouseMoveY);
                ctx.fillStyle = 'rgba(100, 100, 100, 0.5)';
            }
        }, false);
    }



    /**
     * 画像のトリミング
     * リサイズ処理を行っているので
     * 切り取り位置の指定は縮小率を考慮する必要あり
     */
    catImage(): void {
        const oc = <HTMLCanvasElement> document.createElement('canvas');
        const octx = oc.getContext('2d');

        const img = new Image();

        img.onload = (e) => {
            oc.setAttribute('width', (this.catW / this.canvasRatio).toString());
            oc.setAttribute('height', (this.catH / this.canvasRatio).toString());
            octx.drawImage(
                img, // 切り取りイメージ
                (this.mouseStartingPointX / this.canvasRatio), // 切り取り開始X座標
                (this.mouseStartingPointY / this.canvasRatio), // 切り取り開始Y座標
                this.catW / this.canvasRatio, // 切り取り幅
                this.catH / this.canvasRatio, // 切り取り高さ
                0, 0,
                this.catW / this.canvasRatio,
                this.catH / this.canvasRatio); // 切り取り後の表示位置とサイズ
            // 作業履歴保存
            this.setHistory(this.editTarget, this.imageFiles[this.editTarget]);
            // 切り取り後のイメージを保存
            this.imageFiles[this.editTarget]['data'] = oc.toDataURL(this.imageFiles[this.editTarget]['type']);
            this.catOn = false;
            this.reset('last');

            this.imageEditView(this.editTarget);
        };
        img.src = this.imageFiles[this.editTarget]['data'];
    }

    /********************************************
     *
     * 後始末、初期化、その他
     *
     *******************************************/
    /**
     * 表示の切り替え
     * @param event マウスイベント
     */
    switchWindow(event): void {
        if (this.showEditBase) {
            this.onUp = false;
            this.catOn = false;
            this.showEditBase = false;
            this.showUpBase = true;
            this.showBox = false;
            this.toolBox = false;
            this.historyBox = false;
            this.reset('first');
        } else {
            this.onUp = true;
            this.showEditBase = true;
            this.showUpBase = false;
            this.showReload = true;
        }
    }

    /**
     * マウスイベント削除
     * angularに依存せずにイベントを管理しているので必ず
     * イベントを毎回破棄すること
     */

    setHistory(id: number, image: object): void {
        if (!this.imageHistory[id]) {
            this.imageHistory[id] = [];
        }
        if (this.imageHistory.length > 5) {
            this.imageHistory.shift();
        }
        const data = {name: image['name'], type: image['type'], data: image['data']};
        this.imageHistory[id].push(data);
    }
    undoImage(id: number): void {
        this.imageFiles[this.editTarget].name = this.imageHistory[this.editTarget][id].name;
        this.imageFiles[this.editTarget].type = this.imageHistory[this.editTarget][id].type;
        this.imageFiles[this.editTarget].data = this.imageHistory[this.editTarget][id].data;
        const arrCount = this.imageHistory[this.editTarget].length;
        if (arrCount >= id) {
            this.imageHistory[this.editTarget].splice(id, arrCount);
        }
        this.imageEditView(this.editTarget);
    }
    saveImage(id: number, target: string = 'history'): void {
        if (target === 'history') {
            this.imageSaveService.setParam(
                this.imageHistory[this.editTarget][id].name,
                this.imageHistory[this.editTarget][id].type,
                this.imageHistory[this.editTarget][id].data
            );
        } else if (target === 'edit') {
            this.imageSaveService.setParam(
                this.imageFiles[id].name,
                this.imageFiles[id].type,
                this.imageFiles[id].data
            );
        }
        this.imageSaveService.saveImage();
    }
    removeMouseEvent(): void {
        this.canvasBase.removeEventListener('mousedown', (e) => {});
        this.canvasBase.removeEventListener('mouseup',   (e) => {});
        this.canvasBase.removeEventListener('mousemove', (e) => {});
    }
    reset(type: string): void {
        if (type === 'last') {
            this.removeMouseEvent();
        } else if (type === 'first') {
            this.reader = [];
            this.loadedCount = 0;
            this.canvasImage = null;
            this.canvasBase = null;
            this.canvasImage = null;
            this.mouseStartingPointX = null;
            this.mouseStartingPointY = null;
            this.catOn = false;
            this.moveSwitch = false;
        }
    }
    getFileType(file): string {
        const header = file.split(';');
        const type = header[0].split(':');
        return type[1];
    }
    getRandomName(num: number = 16): string {
        return Math.random().toString(36).slice(-(num));
    }

    imageUpLoad(): void {

    }

    close(): void {
        this.subjectsService.publish('closer');
    }
}
