import { Component, HostListener, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';
import 'rxjs/add/operator/switchMap';
// Import Searvice
import { Monster, MonsterService, Arousal, ArousalService } from '../service/index';
import { Skill, SkillService, Leader, LeaderService } from '../service/index';
import { Mtype, MtypeService, Attribute, AttributeService } from '../service/index';
// Import Search Component
import { LeaderSearchComponent, SkillSearchComponent } from '../lib/search/index';
// Import Select Component
import { TypeSelectComponent, AttributeSelectComponent, ArousalSelectComponent } from '../lib/select/index';
// Import Local Share Service
import { LocalDataService } from '../service/local-data.service';
import { SubjectsService } from '../service/subjects.service';
// Import Animation Component
import { slideInOutAnimation } from '../lib/animation/slide-in-out.animation';
import { slideContentAnimation } from '../lib/animation/slide-content.animation';

@Component({
  selector: 'monster-detail',
  templateUrl: './monster-detail.component.html',
  animations: [slideInOutAnimation, slideContentAnimation],
  providers: [ LocalDataService ],
  host: { '[@slideInOutAnimation]': '' }
})

export class MonsterDetailComponent implements OnInit {

    monster: Monster;
    skills: Skill[];
    leaders: Leader[];
    arousals: Arousal[];
    mtypes: Mtype[];
    attributes: Attribute[];

    subWindow: string = 'void';
    showSkillSearch: boolean = false;
    showLeaderSearch: boolean = false;

    searchOn: boolean = true;
    searchType: string = 'none';

    searchContent: any;
    searchComponents: any[] = [
            SkillSearchComponent,
            LeaderSearchComponent,
            TypeSelectComponent,
            AttributeSelectComponent,
            ArousalSelectComponent
        ];

    MonsterTypeTarget: number;
    MonsterAttributeTarget: number;

    subFlag: boolean = true;
    subTop: string   = '0px';
    subLeft: string  = '0px';

    msg: any;
    onCtrl: boolean = false;

    private debug: boolean = true;

    constructor(
        private monsterService: MonsterService,
        private skillService: SkillService,
        private leaderService: LeaderService,
        private arousalService: ArousalService,
        private mtypeService: MtypeService,
        private attributeService: AttributeService,
        private route: ActivatedRoute,
        private router: Router,
        private location: Location,
        private localDataService: LocalDataService,
        private subjectsService: SubjectsService
    ) {
    }

    ngOnInit(): any {
        this.getChMsg();
        this.getSkills();
        this.getLeaders();
        this.getMtypes();
        this.getAttributes();
        this.getArousals();
        this.getRouteParams();
    }


    getChMsg(): any {
        this.localDataService.toParentData$.subscribe(
            msg => {
                if (this.searchType === 'skill') {
                    this.pullConsole('Pearent Skill:' + msg.id);
                    this.monster.Skill = msg.id;
                } else if (this.searchType === 'leader') {
                    this.pullConsole('Pearent Leader:' + msg.id);
                    this.monster.Reader = msg.id;
                } else if (this.searchType === 'type') {
                    this.pullConsole(this.MonsterTypeTarget);
                    if (this.MonsterTypeTarget === 1) {
                        this.monster.Type = msg.id;
                    } else if (this.MonsterTypeTarget === 2) {
                        this.monster.SubType = msg.id;
                    } else if (this.MonsterTypeTarget === 3) {
                        this.monster.ThirdType = msg.id;
                    }

                } else if (this.searchType === 'attribute') {
                    if (this.MonsterAttributeTarget === 1) {
                        this.monster.Attribute = msg.id;
                    } else if (this.MonsterAttributeTarget === 2) {
                        this.monster.SubAttribute = msg.id;
                    }
                } else if (this.searchType === 'arousal') {

                }
                this.closeSearch();
            }
        );
    }
    /**
     * 情報取得
     */
    getRouteParams(): void {
        this.route.params
        .switchMap((params: Params) => this.monsterService.getMonster(+(params['id'] - 1)))
        .subscribe((monster: Monster) => this.monster = monster);
    }
    getSkills(): void {
        this.skillService
            .getSkills()
            .then((skills: Skill[]) => this.skills = skills );
    }
    getLeaders(): void {
        this.leaderService
            .getLeaders()
            .then((leaders: Leader[]) => this.leaders = leaders);
    }
    getArousals(): void {
        this.arousalService
            .getArousals()
            .then((arousals: Arousal[]) => this.arousals = arousals);
    }
    getMtypes(): void {
        this.mtypeService
            .getMtypes()
            .then((mtypes: Mtype[]) => this.mtypes = mtypes);
    }
    getAttributes(): void {
        this.attributeService
            .getAttributes()
            .then((attributes: Attribute[]) => this.attributes = attributes);
    }


    /**
     * 検索処理
     */
    closeSearch(): void {
        this.subWindow = 'hide';
        this.subFlag = true;
        this.subTop = '';
        this.subLeft = '';
    }
    showSearchSkill(e: any): void {
        if (this.subFlag) {
            this.searchContent = this.searchComponents[0];
            this.localDataService.sendToChildSkill(this.skills);
            this.setSearchParam('40%', '20%', 'skill');
        }
    }
    showSearchLeader(e: any): void {
        if (this.subFlag) {
            this.searchContent = this.searchComponents[1];
            this.localDataService.sendToChildLeader(this.leaders);
            this.setSearchParam('40%', '20%', 'leader');
        }
    }
    setSearchParam(top: string, left: string, type: string): void {
        this.subWindow = 'show';
        this.subFlag = false;
        this.subTop = top;
        this.subLeft = left;
        this.searchType = type;
    }

    setSearch(data: any) {
        console.log('Chiled Event Get::' + data.Name);
    }

    /**
     * リスト選択
     */
    showSelectType(tag: number): void {
        if (this.subFlag) {
            this.MonsterTypeTarget = tag;
            this.pullConsole('type tag ' + tag);
            this.searchContent = this.searchComponents[2];
            this.setSearchParam('40%', '20%', 'type');
        }
    }

    showSelectAttribute(tag: number): void {
        if (this.subFlag) {
            this.MonsterAttributeTarget = tag;
            this.pullConsole('attribute tag ' + tag);
            this.searchContent = this.searchComponents[3];
            this.setSearchParam('40%', '20%', 'attribute');
        }
    }

    showSelectArousal(): void {
        if (this.subFlag) {
            this.searchContent = this.searchComponents[4];
            this.setSearchParam('40%', '20%', 'arousal');
        }
    }

    save(): void {
        this.monsterService.update(this.monster)
            .then(() => {
                this.subjectsService.publish('monster-updated', 'test');
                this.goBack();
            });
    }
    goBack(): void {
        this.location.back();
    }

    pullConsole(mess: any): void {
        if (this.debug) {
            console.log(mess);
        }
    }

    @HostListener('document:keydown', ['$event'])
    onKeydownHandler(event: KeyboardEvent): boolean {
        console.log(event);
        let x = event.keyCode;
        if (x === 116  || this.onCtrl && x === 82) {
            return this.refreshNavigation();
        }

        if (x === 224 || x === 17) {
            this.onCtrl = true;
        }

    }
    refreshNavigation(): boolean {
        console.log('KeyDown!');
        this.router.navigate(['/arousal']);
        return false;
    }

    @HostListener('document:keyup', ['$event'])
    onKeyupHandler(event: KeyboardEvent): void {
        const x = event.keyCode;
        this.onCtrl = (x === 224 || x === 17) ? false : false;
    }
}
