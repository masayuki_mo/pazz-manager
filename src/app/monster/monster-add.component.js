"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var router_1 = require("@angular/router");
require("rxjs/add/operator/toPromise");
// Import Service
var index_1 = require("../service/index");
var index_2 = require("../service/index");
var index_3 = require("../service/index");
// Import Search Component
var index_4 = require("../lib/search/index");
// Import Select Component
var index_5 = require("../lib/select/index");
// Import Local Share Service
var local_data_service_1 = require("../service/local-data.service");
var slide_in_out_animation_1 = require("../lib/animation/slide-in-out.animation");
var slide_content_animation_1 = require("../lib/animation/slide-content.animation");
var MonsterAddComponent = (function () {
    function MonsterAddComponent(router, monsterService, skillService, leaderService, arousalService, mtypeService, attributeService, location, localDataService) {
        this.router = router;
        this.monsterService = monsterService;
        this.skillService = skillService;
        this.leaderService = leaderService;
        this.arousalService = arousalService;
        this.mtypeService = mtypeService;
        this.attributeService = attributeService;
        this.location = location;
        this.localDataService = localDataService;
        this.monster = new index_1.Monster;
        this.subWindow = 'void';
        this.showSkillSearch = false;
        this.showLeaderSearch = false;
        this.searchOn = true;
        this.searchType = 'none';
        this.searchComponents = [
            index_4.SkillSearchComponent,
            index_4.LeaderSearchComponent,
            index_5.TypeSelectComponent,
            index_5.AttributeSelectComponent,
            index_5.ArousalSelectComponent
        ];
        this.subFlag = true;
        this.subTop = '0px';
        this.subLeft = '0px';
        this.onCtrl = false;
        this.debug = true;
    }
    MonsterAddComponent.prototype.ngOnInit = function () {
        this.getSkills();
        this.getLeaders();
        this.getMtypes();
        this.getAttributes();
        this.getArousals();
        this.getChMsg();
    };
    MonsterAddComponent.prototype.getChMsg = function () {
        var _this = this;
        this.localDataService.toParentData$.subscribe(function (msg) {
            if (_this.searchType === 'skill') {
                _this.pullConsole(msg.id);
                _this.monster.Skill = msg.id;
            }
            else if (_this.searchType === 'leader') {
                _this.monster.Reader = msg.id;
            }
            else if (_this.searchType === 'type') {
                _this.pullConsole(_this.MonsterTypeTarget);
                if (_this.MonsterTypeTarget === 1) {
                    _this.monster.Type = msg.id;
                }
                else if (_this.MonsterTypeTarget === 2) {
                    _this.monster.SubType = msg.id;
                }
                else if (_this.MonsterTypeTarget === 3) {
                    _this.monster.ThirdType = msg.id;
                }
            }
            else if (_this.searchType === 'attribute') {
                if (_this.MonsterAttributeTarget === 1) {
                    _this.monster.Attribute = msg.id;
                }
                else if (_this.MonsterAttributeTarget === 2) {
                    _this.monster.SubAttribute = msg.id;
                }
            }
            else if (_this.searchType === 'arousal') {
            }
            _this.closeSearch();
        });
    };
    /**
     * 情報取得
     */
    MonsterAddComponent.prototype.getSkills = function () {
        var _this = this;
        this.skillService
            .getSkills()
            .then(function (skills) { return _this.skills = skills; });
    };
    MonsterAddComponent.prototype.getLeaders = function () {
        var _this = this;
        this.leaderService
            .getLeaders()
            .then(function (leaders) { return _this.leaders = leaders; });
    };
    MonsterAddComponent.prototype.getArousals = function () {
        var _this = this;
        this.arousalService
            .getArousals()
            .then(function (arousals) { return _this.arousals = arousals; });
    };
    MonsterAddComponent.prototype.getMtypes = function () {
        var _this = this;
        this.mtypeService
            .getMtypes()
            .then(function (mtypes) { return _this.mtypes = mtypes; });
    };
    MonsterAddComponent.prototype.getAttributes = function () {
        var _this = this;
        this.attributeService
            .getAttributes()
            .then(function (attributes) { return _this.attributes = attributes; });
    };
    /**
     * 検索処理
     */
    MonsterAddComponent.prototype.closeSearch = function () {
        this.subWindow = 'hide';
        this.subFlag = true;
        this.subTop = '';
        this.subLeft = '';
    };
    MonsterAddComponent.prototype.showSearchSkill = function (e) {
        if (this.subFlag) {
            this.searchContent = this.searchComponents[0];
            this.localDataService.sendToChildSkill(this.skills);
            this.setSearchParam('40%', '20%', 'skill');
        }
    };
    MonsterAddComponent.prototype.showSearchLeader = function (e) {
        if (this.subFlag) {
            this.searchContent = this.searchComponents[1];
            this.localDataService.sendToChildLeader(this.leaders);
            this.setSearchParam('40%', '20%', 'leader');
        }
    };
    MonsterAddComponent.prototype.setSearchParam = function (top, left, type) {
        this.subWindow = 'show';
        this.subFlag = false;
        this.subTop = top;
        this.subLeft = left;
        this.searchType = type;
    };
    MonsterAddComponent.prototype.setSearch = function (data) {
        console.log('Chiled Event Get::' + data.Name);
    };
    /**
     * リスト選択
     */
    MonsterAddComponent.prototype.showSelectType = function (tag) {
        if (this.subFlag) {
            this.MonsterTypeTarget = tag;
            this.pullConsole('type tag ' + tag);
            this.searchContent = this.searchComponents[2];
            this.setSearchParam('40%', '20%', 'type');
        }
    };
    MonsterAddComponent.prototype.showSelectAttribute = function (tag) {
        if (this.subFlag) {
            this.MonsterAttributeTarget = tag;
            this.pullConsole('attribute tag ' + tag);
            this.searchContent = this.searchComponents[3];
            this.setSearchParam('40%', '20%', 'attribute');
        }
    };
    MonsterAddComponent.prototype.showSelectArousal = function () {
        if (this.subFlag) {
            this.searchContent = this.searchComponents[4];
            this.setSearchParam('40%', '20%', 'arousal');
        }
    };
    /**
    * 追加
    */
    MonsterAddComponent.prototype.add = function () {
        var _this = this;
        if (!this.monster.id) {
            return;
        }
        this.monsterService.create(this.monster)
            .then(function (monster) {
            _this.monster = monster;
            console.log('new monster add success');
            _this.goBack();
        });
    };
    MonsterAddComponent.prototype.goBack = function () {
        this.location.back();
    };
    MonsterAddComponent.prototype.pullConsole = function (mess) {
        if (this.debug) {
            console.log(mess);
        }
    };
    MonsterAddComponent.prototype.onKeydownHandler = function (event) {
        console.log(event);
        var x = event.keyCode;
        if (x === 116 || this.onCtrl && x === 82) {
            return this.refreshNavigation();
        }
        if (x === 224 || x === 17) {
            this.onCtrl = true;
        }
    };
    MonsterAddComponent.prototype.refreshNavigation = function () {
        console.log('KeyDown!');
        this.router.navigate(['/monster']);
        return false;
    };
    MonsterAddComponent.prototype.onKeyupHandler = function (event) {
        var x = event.keyCode;
        this.onCtrl = (x === 224 || x === 17) ? false : false;
    };
    return MonsterAddComponent;
}());
__decorate([
    core_1.HostListener('document:keydown', ['$event']),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [KeyboardEvent]),
    __metadata("design:returntype", Boolean)
], MonsterAddComponent.prototype, "onKeydownHandler", null);
__decorate([
    core_1.HostListener('document:keyup', ['$event']),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [KeyboardEvent]),
    __metadata("design:returntype", void 0)
], MonsterAddComponent.prototype, "onKeyupHandler", null);
MonsterAddComponent = __decorate([
    core_1.Component({
        selector: 'monster-add',
        templateUrl: './monster-add.component.html',
        animations: [slide_in_out_animation_1.slideInOutAnimation, slide_content_animation_1.slideContentAnimation],
        providers: [local_data_service_1.LocalDataService],
        host: { '[@slideInOutAnimation]': '' }
    }),
    __metadata("design:paramtypes", [router_1.Router,
        index_1.MonsterService,
        index_2.SkillService,
        index_2.LeaderService,
        index_1.ArousalService,
        index_3.MtypeService,
        index_3.AttributeService,
        common_1.Location,
        local_data_service_1.LocalDataService])
], MonsterAddComponent);
exports.MonsterAddComponent = MonsterAddComponent;
//# sourceMappingURL=monster-add.component.js.map