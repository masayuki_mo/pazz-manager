"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
// Import Main Component
var monster_search_component_1 = require("../lib/search/monster-search.component");
// Import Service
var index_1 = require("../service/index");
var index_2 = require("../service/index");
var index_3 = require("../service/index");
// Import TherdParty Service
var index_4 = require("../lib_service/index");
// Import Local Share Service
var local_data_service_1 = require("../service/local-data.service");
var subjects_service_1 = require("../service/subjects.service");
// Import Animation
var fade_in_animation_1 = require("../lib/animation/fade-in.animation");
var slide_content_animation_1 = require("../lib/animation/slide-content.animation");
var MonsterComponent = (function () {
    function MonsterComponent(router, monsterService, skillService, leaderService, arousalService, mtypeService, attributeService, localdataService, subjectsService, csvmakerService, pdfmakerService) {
        this.router = router;
        this.monsterService = monsterService;
        this.skillService = skillService;
        this.leaderService = leaderService;
        this.arousalService = arousalService;
        this.mtypeService = mtypeService;
        this.attributeService = attributeService;
        this.localdataService = localdataService;
        this.subjectsService = subjectsService;
        this.csvmakerService = csvmakerService;
        this.pdfmakerService = pdfmakerService;
        this.navigate = true;
        this.showMonsters = new Array(0);
        this.counts = new Array(0);
        this.start = 0;
        this.len = 10;
        this.subFlag = true;
        this.subWindow = 'void';
    }
    MonsterComponent.prototype.ngOnInit = function () {
        this.getArousals();
        this.getAttributes();
        this.getSkills();
        this.getLeaders();
        this.getMtypes();
        this.getMonsters();
        this.reloadCheck();
        this.getMsg();
    };
    MonsterComponent.prototype.ngOnDestroy = function () {
        this.subscription.unsubscribe();
    };
    MonsterComponent.prototype.pager = function (page) {
        this.start = this.len * page;
        this.showMonsters = [];
        for (var i = this.start; i < (this.len + this.start); i++) {
            this.showMonsters.push(this.monsters[i]);
        }
    };
    MonsterComponent.prototype.getMsg = function () {
        var _this = this;
        this.localdataService.toParentData$.subscribe(function (msg) {
            console.log(msg.Name);
            _this.closeSearch();
            _this.router.navigate(['/monster/detail/', msg.id]);
        });
    };
    MonsterComponent.prototype.reloadCheck = function () {
        var _this = this;
        this.subscription = this.subjectsService
            .on('monster-updated')
            .subscribe(function (msg) {
            console.log('Do update Monsters' + msg);
            _this.getMonsters();
        });
    };
    MonsterComponent.prototype.getMonsters = function () {
        var _this = this;
        this.monsterService
            .getMonsters()
            .then(function (monsters) { return _this.monsters = monsters; })
            .then(function (monsters) { return _this.counts = new Array(Math.ceil(monsters.length / 10)); });
    };
    MonsterComponent.prototype.getSkills = function () {
        var _this = this;
        this.skillService.getSkills()
            .then(function (skills) { return _this.skills = skills; });
    };
    MonsterComponent.prototype.getLeaders = function () {
        var _this = this;
        this.leaderService.getLeaders()
            .then(function (leaders) { return _this.leaders = leaders; });
    };
    MonsterComponent.prototype.getArousals = function () {
        var _this = this;
        this.arousalService.getArousals()
            .then(function (arousals) { return _this.arousals = arousals; });
    };
    MonsterComponent.prototype.getMtypes = function () {
        var _this = this;
        this.mtypeService.getMtypes()
            .then(function (mtypes) { return _this.mtypes = mtypes; });
    };
    MonsterComponent.prototype.getAttributes = function () {
        var _this = this;
        this.attributeService.getAttributes()
            .then(function (attributes) { return _this.attributes = attributes; });
    };
    MonsterComponent.prototype.delete = function (monster) {
        var _this = this;
        this.monsterService
            .delete(monster.id)
            .then(function () {
            _this.monsters = _this.monsters
                .filter(function (_monster) { return _monster !== monster; });
            if (_this.selectedMonster === monster) {
                _this.selectedMonster = null;
            }
        });
    };
    MonsterComponent.prototype.showSearch = function () {
        this.searchContent = monster_search_component_1.MonsterSearchComponent;
        this.subFlag = false;
        this.subWindow = 'show';
    };
    MonsterComponent.prototype.closeSearch = function () {
        // this.searchContent.destroy();
        this.subFlag = true;
        this.subWindow = 'hide';
    };
    MonsterComponent.prototype.gotoDetail = function () {
        this.router.navigate(['./detail-m', this.selectedMonster.id]);
    };
    MonsterComponent.prototype.onSelect = function (monster) {
        this.selectedMonster = monster;
        console.log(this.selectedMonster);
    };
    MonsterComponent.prototype.getcsvFile = function () {
        this.csvmakerService.setCsvName('monster_list');
        var data = this.csvmakerService.convObjectToCSV(this.monsters);
        this.csvmakerService.getCsv(data);
    };
    MonsterComponent.prototype.getPdfFile = function () {
        var dd = {
            content: [
                {
                    text: 'テストテストΦ',
                    style: 'header'
                },
                'Testtesttest now loading loading loading................\n\n',
                {
                    text: 'Testtest subheader',
                    style: 'subheader'
                },
            ],
            styles: {
                header: {
                    fontSize: 18,
                    bold: true
                },
                subheader: {
                    fontSize: 15,
                    bold: true
                },
            }
        };
        var pdf = this.pdfmakerService.createPdf(dd);
        console.log(pdf);
        this.pdfmakerService.createPdfUrl(pdf);
    };
    return MonsterComponent;
}());
MonsterComponent = __decorate([
    core_1.Component({
        selector: 'pazz-monsters',
        templateUrl: './monster.component.html',
        providers: [local_data_service_1.LocalDataService],
        animations: [fade_in_animation_1.fadeInAnimation, slide_content_animation_1.slideContentAnimation],
        host: { '[@fadeInAnimation]': '' }
    }),
    __metadata("design:paramtypes", [router_1.Router,
        index_1.MonsterService,
        index_2.SkillService,
        index_2.LeaderService,
        index_1.ArousalService,
        index_3.MtypeService,
        index_3.AttributeService,
        local_data_service_1.LocalDataService,
        subjects_service_1.SubjectsService,
        index_4.CsvMakerService,
        index_4.PdfMakerService])
], MonsterComponent);
exports.MonsterComponent = MonsterComponent;
//# sourceMappingURL=monster.component.js.map