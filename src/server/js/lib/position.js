var position = {

    hoge:""
    ,WindowSizeW: 0
    ,WindowSizeH: 0
    ,MousePositionX: 0
    ,MousePositionY: 0

    ,targetSizeW: 0
    ,targetSizeH: 0

    ,get: function(e,target,over){

        this.getTargetSize(target)

        this.WindowSizeW = $(window).width();
        this.WindowSizeH = $(window).height();
        this.MousePositionX = e.clientX;
        this.MousePositionY = e.clientY;


        var x = 0;y = 0;
        if(this.WindowSizeW <= this.MousePositionX + over + this.targetSizeW){

            x = this.MousePositionX - this.targetSizeW - over;

            if(Math.floor(this.WindowSizeH / 0.7) <= this.MousePositionY){
                y = this.MousePositionY - this.targetSizeH;
            }else{
                y = this.MousePositionY - (this.targetSizeH / 2);
            }

        }else{
            x = this.MousePositionX + over;

            if(Math.floor(this.WindowSizeH / 0.6) <= this.MousePositionY){
                y = this.MousePositionY - this.targetSizeH - (this.targetSizeH / 2);
            }else{
                y = this.MousePositionY - (this.targetSizeH / 2);
            }
        }

        return [x,y];
    }

    ,getTargetSize: function(target){

        var wid = $(target).css("width").split("px");
        var hei = $(target).css("height").split("px");

        this.targetSizeW = Math.floor(wid[0]);
        this.targetSizeH = Math.floor(hei[0]);
    }
}
