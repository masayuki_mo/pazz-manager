
var InputCheck = {

    Target:""
    ,ViewTarget:""

    ,Weekday: new Array("日","月","火","水","木","金","土")

    ,setup: function(target){
        $("#"+target).focus(function(e){

            DatePicker.Cal(e);
        });
        $("#"+target).blur(function(){
            DatePicker.Close();
        });
    }

    ,Cal: function(target,year,month){

         if(this.Target != target){
             this.Target = target;
             this.ViewTarget = $("#"+target).parents("td");
         }else{
             this.Close();
         }

         var today=new Date();

         if (!year) var year=today.getFullYear();

         if (!month) var month=today.getMonth();
         else month--;

         var day=today.getDate();

         var leap_year=false;
         if ((year%4 == 0 && year%100 != 0) || (year%400 == 0)) leap_year=true;

         lom=new Array(31,28+leap_year,31,30,31,30,31,31,30,31,30,31);

         var days=0;

         for (var i=0; i < month; i++) days+=lom[i];

         var week=Math.floor((year*365.2425+days)%7);
         var j=0;
         var calendar=
         "<div id='PickerCal'>" +
             "<table>\n" +
             "<caption>" +
                 "<div id='PickerCal-head'>" +
                     "<div onclick='DatePicker.Close()'>×</div>" +
                 "<\/div>" +
                 "<div id='PickerCal-title'>" +
                     "<div class='PickerCal-chenge' onclick='DatePicker.BackMonth("+year+","+(month+1)+");'><<<\/div>" +
                     "<div>"+year+"年 "+(month+1)+"月"+"<\/div>" +
                     "<div class='PickerCal-chenge' onclick='DatePicker.NextMonth("+year+","+(month+1)+");'>>><\/div>" +
                 "<\/div>" +
             "<\/caption>\n<tr>";

         for (i=0; i < 7; i++) calendar+="<td>"+this.Weekday[i]+"<\/td>";

         calendar+="<\/tr>\n<tr>";

         for (i=0; i < week; i++,j++) calendar+="<td><\/td>";

         for (i=1; i <= lom[month]; i++) {
             calendar+="<td";
             if (day == i){
                 calendar+=" class='today'";
             }else{
                 calendar+=" class='day'";
             }
             calendar+=" onclick='DatePicker.addDate("+i+");'>"+i+"<\/td>";
             j++;
             if (j > 6) {
                 calendar+="<\/tr>\n<tr>";
                 j=0;
             }
         }

         for (i=j; i > 6; i++) calendar+="<td><\/td>";

         calendar+="<\/tr>\n<\/table><\/div>\n";

         this.Year = year;
         this.Month = (month + 1);

         $(this.ViewTarget).append(calendar);

         $("#PickerCal").css({
            top:($("#"+this.Target).position().top - 80)+"px"
            ,left:($("#"+this.Target).position().left + 60)+"px"
         });
    }

    ,Year: 0
    ,Month: 0
    ,addDate: function(date){
        alert(this.Target);
        var month = (this.Month < 10)?"0"+this.Month:this.Month;
        var day = (date < 10)?"0"+date:date;

        $("#"+this.Target).attr("value",this.Year+"-"+month+"-"+day);
        this.Close();
    }

    ,NextMonth: function(year,month){
        if(month == 12){
            month = 1;
            year++;
        }else{
            month++;
        }

        this.Cal(this.Obj,this.Target,year,month);
    }

    ,BackMonth: function(year,month){
        if(month == 1){
            month = 12;
            year--;
        }else{
            month--;
        }

        this.Cal(this.Obj,this.Target,year,month);
    }

    ,Close: function(){
        $("#PickerCal").remove();
    }
}