
$(document).ready(function(){

    monster.getURL();
    monster.getMonsterData();
    monster.getSkillData();
    monster.getLeaderData();

});

var monster = {

    hoge:""

    ,Skills:{}
    ,Leaders:{}
    ,Monsters:{}
    ,URL:""

    ,Target:""

    ,setEvent:function(){
        $("#ch-skill").bind("click",function(){
            monster.showList("skill");
        })
        $("#ch-leader").bind("click",function(){
            monster.showList("leader");
        })
    }

    ,showList: function(target){
        this.Target = target;
        $("#listview").show();
        word.setFilterEvent();
        $("ul#listbox").empty();
    }
    ,hideList: function(target){
        this.Target = "";
        $("ul#listbox").empty();
        $("#listview").hide();
    }

    ,addList:function(num){
        if(this.Target == 'skill'){
            $("#MonsterSkill").val(num);
            $("p#skill").empty();
            $("p#skill").append(monster.Skills[num]);
        }else if(this.Target == 'leader'){
            $("#MonsterReader").val(num);
            $("p#leader").empty();
            $("p#leader").append(monster.Leaders[num]);
        }

        this.hideList();
    }

    ,getURL: function(){
        this.URL = $("div#access").html();
    }
    //モンスターデータの取得
    ,getMonsterData:function(){
        AjaxLoader.load(
            "job=mons&id=all",
            this.URL+"/testapi",
            "",
            function(target,msg){

                var val = JSON.parse(msg);

                if(val['status']){
                    monster.Monsters = val['result'];
                }else{
                    alert('ERROR'+val['error']['message']);
                }

        });
    }
    //モンスターデータの取得
    ,getSkillData:function(){
        AjaxLoader.load(
            "job=skill&id=all",
            this.URL+"/testapi",
            "",
            function(target,msg){

                var val = JSON.parse(msg);

                if(val['status']){
                    monster.Skills = val['result'];
                }else{
                    alert('ERROR'+val['error']['message']);
                }

        });
    }
    //モンスターデータの取得
    ,getLeaderData:function(){
        AjaxLoader.load(
            "job=lead&id=all",
            this.URL+"/testapi",
            "",
            function(target,msg){

                var val = JSON.parse(msg);

                if(val['status']){
                    monster.Leaders = val['result'];
                }else{
                    alert('ERROR'+val['error']['message']);
                }
                $("p#skill").append(monster.Skills[$("#MonsterSkill").val()]);
                $("p#leader").append(monster.Leaders[$("#MonsterReader").val()]);
                monster.setEvent();

        });
    }
}
