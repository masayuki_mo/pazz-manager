var AjaxLoader = {

    load: function(data,url,target,job,propatie){
        try {



            $.ajax({
                type: "POST",
                url: url,
                dataType: "html",
                data: data,
                success: function(msg){
                    if(typeof(job) === "function"){
                        job(target,msg,propatie);
                    }else{
                        AjaxLoader.defaultJob(target,msg);
                    }
                    if(status != null && status != ""){
                        AjaxLoader.showstatus(status);
                    }
                },
                error: function(e){
                    alert("通信エラー"+e);
                }
            });
        } catch (e) {
            alert("Error::"+e);
        }
    }

    ,loadJson: function(data,url,target,job,propatie){
        try {

            AjaxLoader.showLoadView();

            $.ajax({
                type: "POST",
                url: url,
                dataType: "json",
                data: data,
                success: function(msg){
                    if(typeof(job) === "function"){
                        job(target,msg,propatie);
                    }else{
                        AjaxLoader.defaultJob(target,msg);
                    }
                    if(status != null && status != ""){
                        AjaxLoader.showstatus(status);
                    }
                },
                error: function(e){
                    alert("通信エラー"+e);
                },
                complete: function(){
                    AjaxLoader.closeLoadView();
                }
            });
        } catch (e) {
            alert("Error::"+e);
        }
    }


    ,defaultJob: function(target,msg){
        $(target).fadeOut("fast",function(){
            $(target).children().remove();
            $(target).append(msg);
            $(target).fadeIn("fast");
        });
    }

    ,push_edit: function(key,url,job){
        try {
            $.ajax({
                type: "POST",
                url: url,
                dataType: "html",
                data: key,
                success: function(msg){

                    if(msg == "success"){
                        if(Propaties.Mode === "customer"){
                            searchUser.view();
                        }else if(Propaties.Mode === "company"){
                            searchCompany.view();
                        }
                        AjaxLoader.showstatus("更新完了");
                    }else{
                        searchUser.view();
                        AjaxLoader.showstatus(msg);
                    }
                }
            });
        } catch (e) {
            alert("Error::"+e);
        }

    }

    ,showLoadView: function(){
        $("#loader").append("<img src='../../img/loader.gif'>");
        $("#loader").css({
            position: "absolute",
            top: "0px",
            left: "0px",
            width: "100%",
            height: "100%",
            paddingTop: "28%",
            paddingLeft: "45%",
            backgroundColor: "rgba(250,250,250,0.6)",
            zIndex: "100000"
        });
    }
    ,closeLoadView: function(){
        $("#loader").empty();
        $("#loader").css({
            position: "absolute",
            width: "0px",
            height: "0px",
            paddingTop: "0px",
            paddingLeft: "0px"
        });
    }


    ,showstatus: function(msg){
        Alert.pearent = "#alert-window";
        Alert.status(msg);
    }
}
