
$(document).ready(function(){

    leader.getURL();
    leader.getSkillData();
    $("#listview").show();

});

var leader = {

    hoge:""

    ,Leaders:{}
    ,URL:""

    ,Target:""

    ,setEvent:function(){
        this.setFilterEvent();
    }


    ,getURL: function(){
        this.URL = $("div#access").html();
    }

    //モンスターデータの取得
    ,getSkillData:function(){
        AjaxLoader.load(
            "job=lead&id=all",
            this.URL+"/testapi",
            "",
            function(target,msg){

                var val = JSON.parse(msg);

                if(val['status']){
                    leader.Leaders = val['result'];
                }else{
                    alert('ERROR'+val['error']['message']);
                }

                leader.setEvent();

                $("#ReaderId").val(leader.Leaders.length);
        });
    }

    ,searched: true
    ,setFilterEvent: function(){
        if(!word.setuped){
            $("#ReaderName").bind("keyup",function(){
                if(leader.searched){
                    leader.searched = false;
                    setTimeout(leader.buildList, 500);
                }
            });
        }
    }

    ,buildList: function(){
        $("#listbox").empty();
        String.prototype.toOneNumeric = function(){
            return this.replace(/[Ａ-Ｚａ-ｚ０-９]/g, function(s) {
                return String.fromCharCode(s.charCodeAt(0) - 0xFEE0);
            });
        }
        var w = $("#ReaderName").val();
        var wcheck = w.toOneNumeric();

        var list = leader.Leaders;

        if(isFinite(wcheck) && $("#list-search-word").val() != ""){

            for(var i in list){

                var num = list[i]["id"].indexOf(w)

                if(num >= 0){
                    var m = list[i];
                    $("ul#listbox").append("" +
                            "<li data-role='"+i+"'>" +
                            "<p>"+i+"</p>" +
                            "<p>"+m+"</p>" +
                            "</li>");
                }
            }

        }else if($("#word_search").val() != ""){

            for(var i in list){

                var num = list[i].indexOf(w)

                if(num >= 0){
                    var m = list[i];
                    $("ul#listbox").append("" +
                            "<li data-role='"+i+"'>" +
                            "<p>"+i+"</p>" +
                            "<p>"+m+"</p>" +
                            "</li>");
                }
            }

        }

        return function(){
            leader.searched = true;
        }();
    }

}
