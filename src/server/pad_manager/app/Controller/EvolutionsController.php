<?php

class EvolutionsController extends AppController {

    var $uses = array('Monster', 'Type','Attribute','Arousal','Skill','Reader','Evolution');
    public $helpers = array('Html','Form');

    public function index($page = null) {

        $page = (isset($page))?$page:1;

        $prop['monster'] = $this->Monster->find('list',
                array('fields' => array('Id','Name')));
        $prop['monster'][0] = "";
        $pages = count(($this->Evolution->find('all',
            array(
                'fields' => array('Id')
            )
        )));

        $limit = 100;
        $vb = new ViewBuilder();
        $prop["Count"] = $vb->pageBuild($pages,$limit);

        $prop["Evolution"] = $this->Evolution->find('all',
                array(
                        'limit' => $limit,
                        'page' => $page
                ));
        $this->set('prop',$prop);
    }

    public function view($id = null){
        if(!$id){
            throw new NotFoundException(_('Invalid post'));
        }

        $post = $this->Post->findById($id);

        if (!$post){
            throw new NotFoundException(_('Invalid post'));
        }

        $this->set('post',$post);
    }


    public function add(){
        if($this->request->is('post')){

            $this->Evolution->create();

            for($i = 1; $i <= 6; $i++){
                if($this->request->data["Evolution"]["eam".$i] != 0){
                    $this->request->data["Evolution"]["em".$i] = "";
                    for($j = 0; $j <= 5; $j++){
                        if($j > 0 && $this->request->data["Evolution"]["em".$i."-".$j] > 0){
                            $this->request->data["Evolution"]["em".$i] .= ":";
                        }
                        $this->request->data["Evolution"]["em".$i] .= $this->request->data["Evolution"]["em".$i."-".$j];
                        $this->request->data["Evolution"]["em".$i."-".$j] = null;
                    }
                }else if($this->request->data["Evolution"]["eam".$i] == null){
                    $this->request->data["Evolution"]["eam".$i] =0;
                }
            }
            if($this->Evolution->save($this->request->data)){
                $this->Session->setFlash(_('Yout post has been saved'));
                return $this->redirect(array('action' => 'index'));
            }

            $this->Session->setFlash(_('Unable to add your post.'));
        }

        $prop['count'] = $this->Evolution->find("count");

        $this->set('prop',$prop);
    }

    public function edit($id = null) {
        if (!$id) {
            throw new NotFoundException(__('Invalid post'));
        }

        $post = $this->Evolution->findById($id);
        if (!$post) {
            throw new NotFoundException(__('Invalid post'));
        }

        for($i = 1; $i <= 6; $i++){
            if($post["Evolution"]["em".$i] != 0){

                $ev = explode(':',$post["Evolution"]["em".$i]);

                for($j = 0; $j <= 5; $j++){
                    if(isset($ev[$j])){
                        $post["Evolution"]["em".$i."-".$j] = $ev[$j];
                    }else{
                        $post["Evolution"]["em".$i."-".$j] = "";
                    }
                }
            }
        }

        $prop['monster'] = $this->Monster->find('list',
                array('fields' => array('Id','Name')));
        $prop['monster'][0] = "";

        if ($this->request->is(array('post', 'put'))) {
            $this->Evolution->id = $id;


            for($i = 1; $i <= 5; $i++){
                if($this->request->data["Evolution"]["eam".$i] > 0){

                    $this->request->data["Evolution"]["em".$i] = "";
                    for($j = 0; $j <= 4; $j++){
                        if($this->request->data["Evolution"]["em".$i."-".$j] != null
                            && $j > 0){
                            $this->request->data["Evolution"]["em".$i] .= ":";
                        }
                        $this->request->data["Evolution"]["em".$i] .= $this->request->data["Evolution"]["em".$i."-".$j];
                        $this->request->data["Evolution"]["em".$i."-".$j] = null;
                    }
                }
            }
            if ($this->Evolution->save($this->request->data)) {
                $this->Session->setFlash(__('Your post has been updated.'));
                return $this->redirect(array('action' => 'index'));
            }
            $this->Session->setFlash(__('Unable to update your post.'));
        }

        if (!$this->request->data) {
            $this->request->data = $post;
        }

        $this->set('prop',$prop);
    }


    public function delete($id) {
        if ($this->request->is('get')) {
            throw new MethodNotAllowedException();
        }

        if ($this->Post->delete($id)) {
            $this->Session->setFlash(
                    __('The post with id: %s has been deleted.', h($id))
            );
        } else {
            $this->Session->setFlash(
                    __('The post with id: %s could not be deleted.', h($id))
            );
        }

        return $this->redirect(array('action' => 'index'));
    }
}
