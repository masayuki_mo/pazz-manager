<?php

class ReaderRepository extends DbRepository
{

    var $scheem = "";
    var $layout = array(
            "id"	    =>array("id","int","0",1),
            "Name"		=>array("Name","text","",1),
            "Type"		=>array("Type","int","0",1),
            "Attribute"	=>array("Attribute","int","0",1),
            "MType"		=>array("MType","int","0",1),
            "Rate"		=>array("Rate","int","0",1),
            "Program"	=>array("Program","int","0",1),
            "Propatie"	=>array("Propatie","text","",1),
            "PType"		=>array("PType","int","0",1),
            "Description"=>array("Description","text","",1),
            "etc3"		=>array("etc3","text","",1),
            "etc4"		=>array("etc4","text","",1)
    );

    public function insert($status){

        $status = $this->validate->add($status);

        $sql = "
            INSERT INTO reader(
                id,Name,Type,Attribute,MType,Rate,Program,Propatie,PType,Description,etc3,etc4
                )
            VALUES(
                :id,:Name,:Type,:Attribute,:MType,:Rate,:Program,:Propatie,:PType,:Description,:etc3,:etc4
            )
        ";

        $stmt = $this->execute($sql,array(
                    ':id'		=> $status['id'],
                    ':Name'		=> $status['Name'],
                    ':Type'		=> $status['Type'],
                    ':Attirbute'=> $status['Attirbute'],
                    ':MType'	=> $status['MType'],
                    ':Rate'		=> $status['Rate'],
                    ':Program'	=> $status['Program'],
                    ':Propatie'	=> $status['Propatie'],
                    ':PType'	=> $status['PType'],
                    ':Description'=> $status['Description'],
                    ':etc3'		=> $status['etc3'],
                    ':etc4'		=> $status['etc4'],
                ));
    }

    public function update($status){

        $status = $this->validate->add($status);

        $sql = "
        UPDATE reader SET
            Name			= :Name,
            Type			= :Type,
            Attribute		= :Attirbute,
            MType			= :MType,
            Rate			= :Rate,
            Program			= :Program,
            Propatie		= :Propatie,
            PType			= :PType,
            Description		= :Description,
            etc3			= :etc3,
            etc4			= :etc4
        WHERE
            id				= :id
        ";

        $stmt = $this->execute($sql,array(
                    ':id'		=> $status['id'],
                    ':Name'		=> $status['Name'],
                    ':Type'		=> $status['Type'],
                    ':Attirbute'=> $status['Attirbute'],
                    ':MType'	=> $status['MType'],
                    ':Rate'		=> $status['Rate'],
                    ':Program'	=> $status['Program'],
                    ':Propatie'	=> $status['Propatie'],
                    ':PType'	=> $status['PType'],
                    ':Description'=> $status['Description'],
                    ':etc3'		=> $status['etc3'],
                    ':etc4'		=> $status['etc4'],
        ));
    }

    public function getAll(){
        $sql = "
        SELECT
            id,
            Name,
            Type,
            Attribute,
            MType,
            Rate,
            Program,
            Propatie,
            PType,
            Description,
            etc3,
            etc4
        FROM
            reader
        ";

        return $this->fetchAll($sql,array());
    }

    public function getById($status){
        $sql = "
        SELECT
            id,
            Name,
            Type,
            Attribute,
            MType,
            Rate,
            Program,
            Propatie,
            PType,
            Description,
            etc3,
            etc4
        FROM
            reader
        WHERE
            id		= :id
        ";

        return $this->fetch($sql,array(
                    ':id'		=> $status['id']
        ));
    }

}

