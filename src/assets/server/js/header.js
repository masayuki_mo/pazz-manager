$(document).ready(function(){
    header.setup();
});

var header = {

    hoge:""

    ,slide: false

    ,setup: function(){

        $("#head-linkbox").find("div").each(function(){
            $(this).hover(
                function(){
                    $(this).find("span").show();
                },
                function(){
                    $(this).find("span").hide();
                });
        });
    }

    ,showLink: function(target){
        $(target).find("span").show();
    }

    ,hideLink: function(target){
        $(target).find("span").hide();
    }

    ,move: function(job)
    {
        if(job === "back"){
            if(header.month == "1" || header.month == "01"){
                header.month = 12;
                header.year--;
            }else{
                header.month--;
            }

        }else if(job === "next"){
            if(header.month == "12"){
                header.month = 1;
                header.year++;
            }else{
                header.month++;
            }
        }

        $("#head-date").empty();
        $("#head-date").append("<p>"+header.year+"年 "+header.month+"月</p>");

        $("#callender").empty();
        $("#tmplReserveDateBox").tmpl().appendTo("#callender");

        monthAll.setup();
    }
}