
var Drag = {

    mT: null
    ,dT: {}
    ,dTX: 0
    ,dTY: 0

    ,mX: 0
    ,mY: 0

    ,dObj: false
    ,mDown: false

    ,startDrag: function(target){

        if(this.mT === null){
            this.mT = target;
        }

        $(target).mousedown(function(e){
                Drag.mDown = false;
                Drag.dObj = true;
                Drag.dragSet($(Drag.mT), e);
        });

        $(document).mousemove(function(e){

            if(Drag.dObj){
                $(Drag.mT).css({
                    top:(Drag.dTY - (Drag.mY - e.clientY)),
                    left:(Drag.dTX - (Drag.mX - e.clientX))
                });

                Drag.deselect();
            }

        });

        $(target).mouseup(function(e){
            Drag.dObj = false;
        });
    }

    /*
     * 初期オブジェクト座標の取得
     */
    ,dragSet: function(target,mouse){
        this.mX = mouse.clientX;
        this.mY = mouse.clientY;
        this.dTY = $(target).position().top;
        this.dTX = $(target).position().left;
        this.dT = $(target);
    }
    ,deselect: function() {
        if(window.getSelection){
            window.getSelection().removeAllRanges();
        }else if(document.selection){
            selection.setEndPoint("EndToStart", document.selection.createRange());
            selection.select();
        }

    }
}