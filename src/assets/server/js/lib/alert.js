
var Alert = {

    pearent: ""
    ,tagName: "alert-view"
    ,job: ""

    ,interval: ""

    ,messeage: function(message,job){
        if(this.pearent === null){
            this.pearent = "body";
        }
        if(this.interval !== null){
            clearInterval(this.interval);
        }
        try {
            this.job = job;
            $(this.pearent).append("" +
                    "<div id='"+this.tagName+"'>" +
                    "<div class='"+this.tagName+"-message'><p>"+message+"</p></div>" +
                    "<div class='"+this.tagName+"-choice-box'>" +
                    "	<p class='"+this.tagName+"-choice' onclick='Alert.answer(true)'>はい</p>" +
                    "	<p class='"+this.tagName+"-choice' onclick='Alert.answer(false)'>いいえ</p></div>" +
                    "</div>");

            this.setCss();

            $("#"+this.tagName).fadeIn("normal");

        } catch (e) {
            alert("Erro ::"+e);
        }
    }


    ,answer: function(ans){

        $("#"+this.tagName).fadeOut("slow",function(){
            $("#"+Alert.tagName).remove();
            if(ans){
                Alert.job();
            }
        })
    }

    ,status: function(message,job){
        if(Alert.pearent === null){
            Alert.pearent = $(document);
        }
        $(Alert.pearent).empty();
        $(Alert.pearent).append("" +
                "<div id='"+Alert.tagName+"'>" +
                "<p>"+message+"</p>" +
                "</div>");

        Alert.setCss();

        $("#"+Alert.tagName).fadeIn("slow",function(){
            Alert.interval = setInterval(function(){
                $("#"+Alert.tagName).fadeOut("slow",function(){
                    $("#"+Alert.tagName).remove();
                    clearInterval(Alert.interval);
                })
            },800);
        });
    }

    ,setCss: function(){
        $("#"+Alert.tagName).css({
            display:"none",
            position:"absolute",
            textAlign:"center",
            textShadow:"1px 1px 5px red",
            position:"absolute",
            top:"5%",
            left:"35%",
            width:"400px",
            height:"150px",
            backgroundColor:"white",
            border:"1px",
            borderRadius:"10px",
            boxShadow:"1px 1px 10px orange",
            zIndex:"100"});
        $("."+Alert.tagName+"-message").css({
            position: "absolute",
            width: "100%",
            bottom: "80px",
            margin: "5px 5px"
        });
        $("."+Alert.tagName+"-choice-box").css({
            position: "absolute",
            width: "100%",
            bottom: "15px",
            cursor: "pointer"
        });
        $("."+Alert.tagName+"-choice").css({
            position: "relative",
            float: "left",
            width: "150px",
            height: "20px",
            margin: "0px 25px",
            boxShadow: "1px 1px 0px rgba(100,100,100,0.4)",
            cursor: "pointer"
        });
        $("."+Alert.tagName+"-choice").hover(
            function(){
                $(this).css({
                    backgroundColor: "rgba(100,100,100,0.4)",
                    boxShadow: "-1px -1px 0px rgba(100,100,100,0.8)"
                });
            },function(){
                $(this).css({
                    backgroundColor: "rgba(250,250,250,1)",
                    boxShadow: "1px 1px 0px rgba(100,100,100,0.4)"
                });
            });

        $("#"+Alert.tagName+" > p").css({
            marginTop:"10%"
        });
    }
}




var Al = {

    pearent: "",
    tagName: "alert-view",
    job: "",

    interval: "",

    messeage: function(message,job){
        if(Al.pearent === null){
            Al.pearent = $(document);
        }
        if(Al.interval !== null){
            clearInterval(Al.interval);
        }
        try {
            Al.job = job;
            $(Al.pearent).append("" +
                    "<div id='"+Al.tagName+"'>" +
                    "<div><p>"+message+"</p></div>" +
                    "<div><p class='"+Al.tagName+"-choice' onclick='Alert.answer(true)'>はい</p>" +
                    "<p class='"+Al.tagName+"-choice' onclick='Alert.answer(false)'>いいえ</p></div>" +
                    "</div>");

            Alert.setCss();

            $("#"+Al.tagName).fadeIn("normal");

        } catch (e) {
            alert("Erro ::"+e);
        }
    },


    answer: function(ans){

        $("#"+Al.tagName).fadeOut("slow",function(){
            $("#"+Al.tagName).remove();
            if(ans){
                Al.job();
            }
        })
    },

    status: function(message,job){
        if(Al.pearent === null){
            Al.pearent = $(document);
        }
        $(Al.pearent).empty();
        $(Al.pearent).append("" +
                "<div id='"+Al.tagName+"'>" +
                "<p>"+message+"</p>" +
                "</div>");

        Al.setCss();

        $("#"+Al.tagName).fadeIn("slow",function(){
            Al.interval = setInterval(function(){
                $("#"+Al.tagName).fadeOut("slow",function(){
                    $("#"+Al.tagName).remove();
                    clearInterval(Al.interval);
                })
            },800);
        });
    },

    setCss: function(){
        $("#"+Al.tagName).css({
            display:"none",
            textAlign:"center",
            textShadow:"1px 1px 5px red",
            position:"absolute",
            top:"5%",
            left:"50%",
            width:"400px",
            height:"150px",
            backgroundColor:"white",
            border:"1px",
            borderRadius:"10px",
            boxShadow:"1px 1px 10px orange",
            zIndex:"100"});

        $("."+Al.tagName+"-choice").css({
            position: "relative",
            float: "left",
            width: "150px",
            cursor: "pointer"
        })
        $("#"+Al.tagName+" > p").css({
            marginTop:"10%"
        });
    }
}

