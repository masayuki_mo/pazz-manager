
var skilltype = {

    hoge:""


    ,setup: function(){
        this.showSkillList();
        this.setFilterEvent();
    }

    ,showSkillList: function(){

        $("#skill-box").empty();

        for(var i in skill.skills){

            if(i > 0){

                var ss = skill.skills[i];

                if(this.dmapFilter[i] == undefined){
                    this.dmapFilter[i] = 0;
                }

                if(this.dmapFilter[i] < 1){
                         $("#skill-box").append("" +
                            "<li>" +
                            " <div>" +
                            " 	<p class='dm-name'>"+ss["Name"]+"</p>" +
                            " 	<p class='dm-skill'>"+ss["Description"]+"</p>" +
                            " 	<p class='dm-start'>初期："+ss["Start"]+"</p>" +
                            " 	<p class='dm-max'>最大："+ss["Max"]+"</p>" +
                            " </div>" +
                            " <div class='dm-monster-list'>"+this.buildMonsterList(i)+
                            " </div>" +
                            "</li>");
                }
            }
        }

        return function(){
            skilltype.setViewEvent();
        }();
    }

    ,tfilter:[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
    ,afilter:[0,0,0,0,0,0,0,0,0]
    ,cfilter:[0,0,0,0,0,0,0,0,0,0,0,0]
    ,dmapFilter:[]

    ,setFilterEvent: function(){
        $("#ss-stype > li").each(function(){
            $(this).bind("click",function(){
                var n = $(this).index() + 1;
                if(!skilltype.tfilter[n]){
                    $(this).css({backgroundColor:"rgba(220,150,100,0.6)"});
                    skilltype.tfilter[n] = 1;
                    skilltype.filterList(0,n,0);
                }else{
                    $(this).css({backgroundColor:"rgba(150,150,150,0.1)"});
                    skilltype.tfilter[n] = 0;
                    skilltype.filterList(0,n,1);
                }
            })
        })
        $("#ss-attribute > li").each(function(){
            $(this).bind("click",function(){
                var n = $(this).index() + 1;
                if(!skilltype.afilter[n]){
                    $(this).css({backgroundColor:"rgba(220,150,100,0.6)"});
                    skilltype.afilter[n] = 1;
                    skilltype.filterList(1,n,0);
                }else{
                    $(this).css({backgroundColor:"rgba(150,150,150,0.1)"});
                    skilltype.afilter[n] = 0;
                    skilltype.filterList(1,n,1);
                }
            })
        })
        $("#ss-type > li").each(function(){
            $(this).bind("click",function(){
                var n = $(this).index() + 1;
                if(!skilltype.cfilter[n]){
                    $(this).css({backgroundColor:"rgba(220,150,100,0.6)"});
                    skilltype.cfilter[n] = 1;
                    skilltype.filterList(1,n,0);
                }else{
                    $(this).css({backgroundColor:"rgba(150,150,150,0.1)"});
                    skilltype.cfilter[n] = 0;
                    skilltype.filterList(1,n,1);
                }
            })
        })
    }
    ,filterList: function(val,num,s){
        for(var i in skill.skills){

            if(i != 0){

                var ss = skill.skills[i];

                if(val){
                    var fil = ss["Type"].split(":");
                }else{
                    var fil = ss["Type"].split(":");
                }

                if(fil.indexOf(String(num)) < 0){
                    if(s){
                        this.dmapFilter[i]--;
                    }else{
                        this.dmapFilter[i]++;
                    }
                }
            }
        }

        return function(){
            skilltype.showSkillList();
        }();
    }

    ,buildMonsterList: function(num){
        var ss = skill.skills[num];

        var check = true;
        var st = 0;
        var list = "";

        while(check){

            var mons = this.showMonster(ss["id"],st);

            if(mons != false){
                list = list + "<p class='m"+mons+"' data-role='"+mons+"' ></p>"
                st = mons;
            }else{
                check = false;
            }
        }

        return list;
    }

    ,showMonster: function(sk,num){
        var n = monster.skillMap.indexOf(sk,num);
        if(n >= 0){
            return n + 1;
        }else{
            return false;
        }
    }

    ,setViewEvent: function(){
        $("ul#skill-box li").find(".dm-monster-list").each(function(){
            $(this).find("p").each(function(){
                $(this).bind("click",function(){
                    monster.buildView(this);
                })
            })
        })
    }

}
