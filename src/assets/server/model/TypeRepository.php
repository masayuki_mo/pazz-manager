<?php

class TypeRepository extends DbRepository
{

    var $scheem = "";
    var $layout = array(
            "id"		    =>array("id","int","0",1),
            "Name"			=>array("Name","text","",1),
            "Description"	=>array("Description","text","",1),
            "etc3"			=>array("etc3","text","",1),
            "etc4"			=>array("etc4","text","",1)
    );

    public function insert($status){

        $status = $this->validate->add($status);

        $sql = "
            INSERT INTO type(
                id,Name,Description,etc3,etc4
                )
            VALUES(
                :id,:Name,:Description,:etc3,:etc4
            )
        ";

        $stmt = $this->execute($sql,array(
                    ':id'			=> $status['id'],
                    ':Name'			=> $status['Name'],
                    ':Description'	=> $status['Description'],
                    ':etc3'			=> $status['etc3'],
                    ':etc4'			=> $status['etc4']
                ));
    }

    public function update($status){

        $status = $this->validate->add($status);

        $sql = "
        UPDATE type SET
            Name		= :Name,
            Start		= :Start,
            Description	= :Description,
            etc3		= :etc3,
            etc4		= :etc4
        WHERE
            id			= :id
        ";

        $stmt = $this->execute($sql,array(
                    ':id'			=> $status['id'],
                    ':Name'			=> $status['Name'],
                    ':Description'	=> $status['Description'],
                    ':etc3'			=> $status['etc3'],
                    ':etc4'			=> $status['etc4']
        ));
    }

    public function getAll(){
        $sql = "
        SELECT
            id,
            Name,
            Description,
            etc3,
            etc4
        FROM
            type

        ";

        return $this->fetchAll($sql,array());
    }


    public function getById($status){
        $sql = "
        SELECT
            id,
            Name,
            Description,
            etc3,
            etc4
        FROM
            type
        WHERE
            id		= :id
        ";

        return $this->fetch($sql,array(
                ':id'			=> $status['id'],
        ));
    }

}

