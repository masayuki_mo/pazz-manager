
<h1>Blog posts</h1>
<table>
    <tr>
        <td>
<p><?php echo $this->Html->link("モンスター", array('action' => '../monsters')); ?></p>
        </td>
        <td>
<p><?php echo $this->Html->link("スキル", array('action' => '..//skills')); ?></p>
        </td>
        <td>
<p><?php echo $this->Html->link("リーダースキル", array('action' => '..//readers')); ?></p>
        </td>
        <td>
<p><?php echo $this->Html->link("進化表", array('action' => '..//evolutions')); ?></p>
        </td>
        <td>
<p><?php echo $this->Html->link("変換表", array('action' => '..//dropmaps')); ?></p>
        </td>
    </tr>
</table>
<table>
    <tr>
<?php $i = 0;
foreach ($monsters["Count"] as $page): ?>
        <?php if(($i % 10) == 0): ?>
    </tr>
    <tr>
        <?php endif; ?>
        <td>
        <?php echo $this->Html->link($page['desc'], array('action' => 'index', $page['page'])); ?>
        </td>
        <?php $i++; ?>
<?php endforeach; ?>
    </tr>
</table>

<p><?php echo $this->Html->link("新規追加", array('action' => 'add')); ?></p>

<table>
    <tr>
        <th>Id</th>
        <th>名前</th>
        <th>編集</th>
        <th>属性・タイプ</th>
        <th>レア・コスト</th>
        <th>ステータス</th>
        <th>スキル</th>
        <th>リーダー</th>
        <th>覚醒</th>
        <th>進化</th>
    </tr>

<!-- $post配列をループして、投稿記事の情報を表示 -->

<?php foreach ($monsters["monster"] as $monster): ?>
    <tr>
        <td><?php echo $monster['Monster']['Id']; ?></td>
        <td>
            <?php echo $this->Html->link($monster['Monster']['Name'], array('action' => 'view', $monster['Monster']['Id'])); ?>
        </td>
        <td>
            <?php echo $this->Form->postLink(
                '削除',
                array('action' => 'delete', $monster['Monster']['Id']),
                array('confirm' => 'Are you sure?'));
            ?>
            <?php echo $this->Html->link('編集', array('action' => 'edit', $monster['Monster']['Id'])); ?>
        </td>
        <td>属：
            <?php echo $monster['Monster']['Attribute']; ?>
            副属：
            <?php echo $monster['Monster']['SubAttribute']; ?>
            タイプ1：
            <?php echo $monster['Monster']['Type']; ?>
            タイプ2：
            <?php echo $monster['Monster']['SubType']; ?>
            タイプ3：
            <?php echo $monster['Monster']['ThirdType']; ?>
        </td>
        <td>
            レア：
            <?php echo $monster['Monster']['Rarity']; ?>
            コスト：
            <?php echo $monster['Monster']['Cost']; ?>
        </td>
        <td>
            HP:<?php echo $monster['Monster']['HP']; ?>
            AT:<?php echo $monster['Monster']['Attack']; ?>
            Cu：<?php echo $monster['Monster']['Cure']; ?>
        </td>
        <td>
            <?php echo $monster['Monster']['Skill']; ?>
        </td>
        <td>
            <?php echo $monster['Monster']['Reader']; ?>
        </td>
        <td>
            <?php echo $monster['Monster']['Arousal']; ?>
        </td>
        <td>
            <?php echo $monster['Monster']['Evolution']; ?>
        </td>
    </tr>
<?php endforeach; ?>

</table>

