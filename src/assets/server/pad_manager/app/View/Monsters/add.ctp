<!-- File: /app/View/Monsters/add.ctp -->

<?php

echo $this->Html->charset();

echo $this->Html->script( '//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js',array('inline'=>false));
echo $this->Html->script( 'ajaxloader.js');
echo $this->Html->script( 'jquery.tmpl.js');
echo $this->Html->script( 'word.js');
echo $this->Html->script( 'monster.js');

echo $this->Html->css( 'listview.css');


?>

<h1>モンスター情報編集</h1>
<?php
echo $this->Form->create('Monster');
echo $this->Form->input('Id', array('label' => '番号'));
echo $this->Form->input('Name',array('label' => '名前'));
echo $this->Form->input('Attribute',array(
        'legend'    => '主属性',
        'type'      => 'radio',
        'options'   => $prop['attribute'],
        'div'       => 'radio-horizon',
        'style'     => 'float:none;',
    ));


echo $this->Form->input('SubAttribute',array(
        'legend'    => '副属性',
        'type'      => 'radio',
        'options'   => $prop['attribute'],
        'div'       => 'radio-horizon',
        'style'     => 'float:none;',
    ));

echo $this->Form->input('Type',array(
        'legend'    => 'タイプ1',
        'type'      => 'radio',
        'options'   => $prop['type'],
        'div'       => 'radio-horizon',
        'style'     => 'float:none;',
    ));

echo $this->Form->input('SubType',array(
        'legend'    => 'タイプ2',
        'type'      => 'radio',
        'options'   => $prop['type'],
        'div'       => 'radio-horizon',
        'style'     => 'float:none;',
    ));

echo $this->Form->input('ThirdType',array(
        'legend'    => 'タイプ3',
        'type'      => 'radio',
        'options'   => $prop['type'],
        'div'       => 'radio-horizon',
        'style'     => 'float:none;',
    ));
?>
<table>
    <tr>
        <td>
<?php
        echo $this->Form->input('Rarity',array('label' => 'レア度'));
?>
        </td>
        <td>
<?php
echo $this->Form->input('Cost',array('label' => 'コスト'));
?>
        </td>
        <td>
<?php
echo $this->Form->input('HP',array('label' => 'HP'));
?>
        </td>
        <td>
<?php
echo $this->Form->input('Attack',array('label' => '攻撃力'));
?>
        </td>
        <td>
<?php
echo $this->Form->input('Cure',array('label' => '回復力'));
?>
        </td>
    </tr>
</table>
<?php
echo $this->Form->input('Skill',array('label' => 'スキル','type' => 'hidden'));
?>
<table>
    <tr>
        <td>スキル</td>
        <td><p id="skill"></p></td>
        <td><p id="ch-skill">変更</p></td>
    </tr>
</table>
<?php
echo $this->Form->input('Reader',array('label' => 'リーダースキル','type' => 'hidden'));
?>
<table>
    <tr>
        <td>リーダースキル</td>
        <td><p id="leader"></p></td>
        <td><p id="ch-leader">変更</p></td>
    </tr>
</table>

<div id="listview">
    <input type='text' id="list-search-word" >
    <ul id="listbox"></ul>
</div>

<table>
    <tr>
<?php
$i = 1;
foreach ($prop['arousal_val'] as $arousal):
?>
        <td>
<?php
    echo $this->Form->input( 'Arousal'.$i, array(
        'label' => '覚醒'.$i,
        'type' => 'select',
        'options' => $prop['arousal'],
        'div' => true,
        'size' => 1,
        'empty' => true
    ));
    $i++;
?>
        </td>
<?php
if($i == 6){
?>
    </tr>
    <tr>
<?php
}

endforeach;
?>
    </tr>
</table>
<?php
echo $this->Form->input( 'Evolution', array(
    'label' => '進化表',
    'type' => 'select',
    'options' => $prop['evolution'],
    'div' => true,
    'size' => 5,
    'empty' => true
));

echo $this->Form->end('Save Post');
?>

<div id="access"><?php echo $this->Html->url('/api',true); ?></div>