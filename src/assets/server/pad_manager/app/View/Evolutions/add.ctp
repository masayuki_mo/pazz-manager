<!-- File: /app/View/Evolution/add.ctp -->
<?php

echo $this->Html->charset();

echo $this->Html->script( '//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js',array('inline'=>false));
echo $this->Html->script( 'ajaxloader.js');
echo $this->Html->script( 'jquery.tmpl.js');
echo $this->Html->script( 'word.js');
echo $this->Html->script( 'evol.js');

echo $this->Html->css( 'listview.css');


?>

<h1>進化表追加</h1>
<?php
echo $this->Form->create('Evolution');
echo $this->Form->input('Id', array('label' => '番号'));
echo $this->Form->input('tmp',array('label' => 'テンプレート','value'=>$prop["count"]));

echo $this->Form->input('eb',array('type' => 'hidden'));

?>
<div onclick="evol.showList(this)" class="ev-mons"><p>進化前</p><p id="eb"></p></div>
<?php


for($i = 1; $i <= 5; $i++){
?>

<table>
    <tr>
<?php
    for($j = 0; $j <= 4; $j++){
?>
        <td>
<?php
    if(isset($this->Form->data['Evolution']['em'.$i][$j])){
        $evmonster = $this->Form->data['Evolution']['em'.$i][$j];
    }else{
        $evmonster = 0;
    }
    echo $this->Form->input('em'.$i."-".$j,array('type' => 'hidden'));
?>
            <div onclick="evol.showList(this)" class="ev-mons"><p>素材</p><p id="<?php echo "em".$i."-".$j ?>"></p></div>
        </td>
<?php
    }
?>
    </tr>
    <tr>
        <td>
<?php
    echo $this->Form->input('eam'.$i,array('type' => 'hidden'));
?>
            <div onclick="evol.showList(this)" class="ev-mons"><p>進化後</p><p id="<?php echo "eam".$i ?>"></p></div>
        </td>
<?php
}
?>
    </tr>
</table>

<?php
echo $this->Form->end('追加');

?>
<div id="evol-list">
<ul>
    <li onclick="evol.addList(147)">炎の番人</li>
    <li onclick="evol.addList(148)">水の番人</li>
    <li onclick="evol.addList(149)">緑の番人</li>
    <li onclick="evol.addList(150)">光の番人</li>
    <li onclick="evol.addList(151)">闇の番人</li>
    <li onclick="evol.addList(321)">虹の番人</li>
    <li onclick="evol.addList(1176)">黄金の番人</li>
</ul>
<ul>
<li onclick="evol.addList(161)">進化の赤仮面</li>
<li onclick="evol.addList(162)">進化の青仮面</li>
<li onclick="evol.addList(163)">進化の緑仮面</li>
<li onclick="evol.addList(164)">進化の黄仮面</li>
<li onclick="evol.addList(165)">進化の紫仮面</li>
</ul>
<ul>
<li onclick="evol.addList(166)">朱色の鬼神面</li>
<li onclick="evol.addList(167)">蒼色の鬼神面</li>
<li onclick="evol.addList(168)">碧色の鬼神面</li>
<li onclick="evol.addList(169)">黄金の鬼神面</li>
<li onclick="evol.addList(170)">紫色の鬼神面</li>
</ul>
<ul>
<li onclick="evol.addList(171)">神化の紅面</li>
<li onclick="evol.addList(172)">神化の蒼面</li>
<li onclick="evol.addList(173)">神化の碧面</li>
<li onclick="evol.addList(174)">神化の金面</li>
<li onclick="evol.addList(175)">神化の黒面</li>
<li onclick="evol.addList(234)">神秘の仮面</li>
<li onclick="evol.addList(1294)">古代の蒼神面</li>
<li onclick="evol.addList(1295)">古代の壁神面</li>
</ul>
<ul>
<li onclick="evol.addList(152)">ドラゴンシード</li>
<li onclick="evol.addList(153)">ドラゴンプラント</li>
<li onclick="evol.addList(154)">ドラゴンフラワー</li>
<li onclick="evol.addList(227)">ドラゴンフルーツ</li>
<li onclick="evol.addList(1085)">ﾚｯﾄﾞﾄﾞﾗｺﾞﾝﾌﾙｰﾂ</li>
<li onclick="evol.addList(1086)">ﾌﾞﾙｰﾄﾞﾗｺﾞﾝﾌﾙｰﾂ</li>
<li onclick="evol.addList(1087)">ｸﾞﾘｰﾝﾄﾞﾗｺﾞﾝﾌﾙｰﾂ</li>
</ul>
<ul>
<li onclick="evol.addList(155)">ルビリット</li>
<li onclick="evol.addList(156)">サファリット</li>
<li onclick="evol.addList(157)">エメリット</li>
<li onclick="evol.addList(158)">トパリット</li>
<li onclick="evol.addList(159)">アメリット</li>
<li onclick="evol.addList(160)">ミスリット</li>
</ul>
<ul>
<li onclick="evol.addList(246)">ダブルビリット</li>
<li onclick="evol.addList(247)">ダブサファリット</li>
<li onclick="evol.addList(248)">ダブエメリット</li>
<li onclick="evol.addList(249)">ダブトパリット</li>
<li onclick="evol.addList(250)">ダブアメリット</li>
<li onclick="evol.addList(251)">ダブミスリット</li>
<li></li>
<li onclick="evol.addList(915)">エンジェリット</li>
<li onclick="evol.addList(916)">デビリット</li>

</ul>
<div id="word-box">
    <input type='text' id="list-search-word" >
    <ul id="listbox"></ul>
</div>
<div onclick="evol.hideList()" class="close-bottom">閉じる</div>
</div>

<div id="evol-temp">
    <div>進化テンプレート</div>
    <div>
        <ul>
            <li onclick="evol.addTemp('1-1')">1-1：元→進化</li>
            <li onclick="evol.addTemp('1-2')">1-2：元→進化→進化</li>
            <li onclick="evol.addTemp('1-3')">1-3：元→進化→進化→進化</li>
            <li></li>
            <li onclick="evol.addTemp('2-1')">2-1：元→進化→究極</li>
            <li onclick="evol.addTemp('2-2')">2-2：元→進化→進化→究極</li>
            <li onclick="evol.addTemp('2-3')">2-3：元→進化→進化→進化→究極</li>
            <li></li>
            <li onclick="evol.addTemp('3-1')">3-1：元→究極</li>
            <li onclick="evol.addTemp('3-2')">3-2：元→究極→究極</li>
            <li></li>
            <li onclick="evol.addTemp('4-1')">4-1：元→進化→究極１　究極２</li>
            <li onclick="evol.addTemp('4-2')">4-2：元→進化→究極１　究極２　究極３</li>
            <li onclick="evol.addTemp('4-3')">4-3：元→進化→究極１　究極２　究極３　究極４</li>
            <li onclick="evol.addTemp('4-4')">4-4：元→進化→究極１　究極２　究極３　究極４　究極５</li>
            <li onclick="evol.addTemp('4-5')">4-5：元→進化→究極１　究極２　覚醒</li>
            <li></li>
            <li onclick="evol.addTemp('5-1')">5-1：元→進化→進化→究極１　究極２</li>
            <li></li>
            <li onclick="evol.addTemp('6-1')">6-1：元→究極１　究極２　究極３</li>
        </ul>
    </div>
</div>



<div id="access"><?php echo $this->Html->url('/api',true); ?></div>