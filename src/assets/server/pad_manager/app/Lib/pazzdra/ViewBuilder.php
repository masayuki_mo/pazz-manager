<?php
class ViewBuilder{



    public function pageBuild($num,$limit){

        $pages = array();
        $pcount = $num / $limit;

        for($i = 0; $i <= $pcount; $i++){

            $pages[$i]["page"] = $i+1;
            $pages[$i]["desc"] = ($limit * $i)."～".($limit * ($i + 1));

        }

        if(($num % $limit) > 0){
            $pages[$i + 1]["page"] = $i+2;
            $pages[$i + 1]["desc"] = ($limit * ($i+1))."～".($limit * ($i + 2));
        }

        return $pages;
    }
}