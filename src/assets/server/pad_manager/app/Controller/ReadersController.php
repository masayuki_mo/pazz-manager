<?php

class ReadersController extends AppController {


    public $helpers = array('Html','Form');

    public function index($page = null) {

        $page = (isset($page))?$page:1;

        $pages = count(($this->Reader->find('all',
            array(
                'fields' => array('Id')
            )
        )));

        $limit = 100;
        $vb = new ViewBuilder();
        $skills["Count"] = $vb->pageBuild($pages,$limit);

        $skills["Reader"] = $this->Reader->find('all',
                array(
                        'limit' => $limit,
                        'page' => $page
                ));
        $this->set('skills',$skills);
    }

    public function view($id = null){
        if(!$id){
            throw new NotFoundException(_('Invalid post'));
        }

        $post = $this->Post->findById($id);

        if (!$post){
            throw new NotFoundException(_('Invalid post'));
        }

        $this->set('post',$post);
    }


    public function add(){
        if($this->request->is('post')){

            $this->Reader->create();

            if($this->Reader->save($this->request->data)){
                $this->Session->setFlash(_('Yout post has been saved'));
                return $this->redirect(array('action' => 'index'));
            }

            $this->Session->setFlash(_('Unable to add your post.'));
        }
    }

    public function edit($id = null) {
        if (!$id) {
            throw new NotFoundException(__('Invalid post'));
        }

        $post = $this->Reader->findById($id);
        if (!$post) {
            throw new NotFoundException(__('Invalid post'));
        }

        if ($this->request->is(array('post', 'put'))) {
            $this->Reader->id = $id;
            if ($this->Reader->save($this->request->data)) {
                $this->Session->setFlash(__('Your post has been updated.'));
                return $this->redirect(array('action' => 'index'));
            }
            $this->Session->setFlash(__('Unable to update your post.'));
        }

        if (!$this->request->data) {
            $this->request->data = $post;
        }
    }
    public function delete($id) {
        if ($this->request->is('get')) {
            throw new MethodNotAllowedException();
        }

        if ($this->Post->delete($id)) {
            $this->Session->setFlash(
                    __('The post with id: %s has been deleted.', h($id))
            );
        } else {
            $this->Session->setFlash(
                    __('The post with id: %s could not be deleted.', h($id))
            );
        }

        return $this->redirect(array('action' => 'index'));
    }
}
